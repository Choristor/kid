﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace WebLayer.DTO
{
	public class AutoMapperConfig
	{
		public static IMapper GetMapper()
		{
			var mapper = new MapperConfiguration(cfg =>
			 {
				 cfg.CreateMap<DATA.EXERCISE, Models.DolgozatModel>().ReverseMap();
				 cfg.CreateMap<DATA.USER, Models.USERModel>().ReverseMap();
				 cfg.CreateMap<DATA.QUESTION, Models.QUESTIONModel>().ReverseMap();
				 cfg.CreateMap<DATA.SUBJECT, Models.TantargyModel>().ReverseMap();
				 cfg.CreateMap<DATA.RESPONSE, Models.RESPONSEModel>().ReverseMap();
				 cfg.CreateMap<DATA.CLASS, Models.OsztalyModel>().ReverseMap();
			 });
			return mapper.CreateMapper();
		}
	}
}