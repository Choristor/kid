
﻿using AutoMapper;
using BusinessLogic;
using DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebLayer.DTO;
using WebLayer.Models;

namespace WebLayer.Controllers
{
	public class ClassController : Controller
	{
		ILogic logic;
		IMapper mapper;
		protected override IAsyncResult BeginExecute(RequestContext requestContext, AsyncCallback callback, object state)
		{
			mapper = AutoMapperConfig.GetMapper();
			logic = FakeLogic.getInstance();
			return base.BeginExecute(requestContext, callback, state);
		}
		// GET: Class
		public ActionResult Index()
		{
			ClassViewModel cVM = new ClassViewModel() { ClassList = GetClasses() };
			return View(cVM);
		}
		[HttpPost]
		public ActionResult Index(ClassViewModel ViewModel)
		{
			ViewModel.ClassList = GetClasses();
			DATA.CLASS theClass = logic.ClassesGetAll().ToList()[1];
			IQueryable<DATA.USER> users = logic.GetStudentsByClass(theClass);
			List<USERModel> diakok = mapper.Map<IQueryable<DATA.USER>, List<USERModel>>(users);
			ViewModel.UserList = diakok;
			return View(ViewModel);
		}
		private List<OsztalyModel> GetClasses()
		{
			IQueryable<DATA.CLASS> classes = logic.ClassesGetAll();
			var ModelClasses = mapper.Map<IQueryable<DATA.CLASS>, List<OsztalyModel>>(classes);
			return ModelClasses;
		}
		[ChildActionOnly]
		public PartialViewResult GetMate(string USER_ID)
		{
			ClassMateModel cmm = new ClassMateModel();
			DATA.USER theUser = logic.USERAdatai(0);
			cmm.classMate = mapper.Map<DATA.USER, USERModel>(theUser);

			IQueryable<EXERCISE> theTest = logic.ExercisesGetByUser(theUser);
			cmm.tests = mapper.Map<IQueryable<EXERCISE>, List<DolgozatModel>>(theTest);
			return PartialView("ClassMate", cmm);
		}
		[HttpPost]
		public ActionResult ToCorrect(USER user)
		{
			//string ddlTestValue = Request.Form["ddlTest"];

			//CorrectTestViewModel ctVM = new CorrectTestViewModel() { questions = logic.QUESTIONekLekereseDolgozatAlapjan(int.Parse(ddlTestValue)).ToList(), user = ViewBag.User };

			return View("CorrectList", user);
		}
	}
}