﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebLayer.Models;

namespace WebLayer.Controllers {
	public class HomeController : Controller {
		ILogic logic;

		protected override void OnActionExecuting(ActionExecutingContext filterContext) {
			logic =FakeLogic.getInstance();
			base.OnActionExecuting(filterContext);
		}
		[AllowAnonymous]
		public ActionResult Index() {
			return PartialView();
		}
		[HttpPost]
		[AllowAnonymous]
		public ActionResult Index(object model){
			string tbUserNameValue = Request.Form["tbUserName"];
			string tbUserPassValue = Request.Form["tbUserPass"];
			if (logic.Login(tbUserNameValue, tbUserPassValue).USER_ID >-1) {
				

				return RedirectToAction("Index", "Profile");

			} else {
				return View();
			}
			
		}
		
		public ActionResult About() {
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact() {
			ViewBag.Message = "Your contact page.";

			return View();
		}
	}
}