﻿using BusinessLogic;
using DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebLayer.Models;

namespace WebLayer.Controllers
{
    public class TestController : Controller
    {
		ILogic logic;
		protected override IAsyncResult BeginExecute(RequestContext requestContext, AsyncCallback callback, object state)
		{
			logic = FakeLogic.getInstance();
			return base.BeginExecute(requestContext, callback, state);
		}
		// GET: Test
		public ActionResult Index()
        {
			SubjectListViewModels slVM = new SubjectListViewModels() { subjects = logic.SubjectsGetAll().ToList() };
            return View(slVM);
        }
		[HttpPost]
		public ActionResult CreateTest(SUBJECT selectedSubject)
		{
			List<QUESTION> questionList=logic.GetRandomQuestions(2,selectedSubject).ToList();
			return View("CreateTest",questionList);
		}

		public ActionResult GetAddedQuestions(QUESTION question)
		{

			return PartialView("AddedQuestion",question);
		}
		public ActionResult PositionUp()
		{
			return View();
		}
		public ActionResult PositionDown()
		{
			return View();
		}
		public ActionResult GetOut()
		{
			return View();
		}
    }
}