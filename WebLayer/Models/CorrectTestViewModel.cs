﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DATA;
namespace WebLayer.Models
{
	public class CorrectTestViewModel
	{
		public USER user { get; set; }
		public List<QUESTION> questions { get; set; }
	}
}