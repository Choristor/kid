﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLayer.Models {
	public class ClassMateModel {
		
		public USERModel classMate { get; set; }
		public List<DolgozatModel> tests { get; set; }
		public String SelectedTestID { get; set; }
	}
}