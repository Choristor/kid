﻿using DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLayer.Models
{
	public class SubjectListViewModels
	{
		public List<SUBJECT> subjects { get; set; }
		public SUBJECT selectedSubject { get; set; }
	}
}