﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLayer.Models
{
	public class USERModel
	{
		public int USER_ID {
			get;set;
		}

		public string USER_NAME{
			get;set;
		}
		public string USER_PASSWORD { get; set; }

		public int USER_CLASS {
			get;set;
		}

		public DateTime USER_BDATE {
			get;set;
		}
		public int USER_AUTH { get; set; }
	}
}