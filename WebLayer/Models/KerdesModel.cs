﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLayer.Models
{
	public class QUESTIONModel
	{
		public int QUESTION_ID {
			get; set;
		}

		public string QUESTION_CONTENT {
			get; set;
		}

		public int QUESTION_TIPUS {
			get; set;
		}

		public int QUESTION_SUBJECT_ID {
			get; set;
		}

		public int QUESTION_UPLOADERUSER_ID {
			get; set;
		}

		public int QUESTION_SCORE {
			get;set;
		}
	}
}