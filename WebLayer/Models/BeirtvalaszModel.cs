﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLayer.Models
{
	public class RESPONSEModel
	{
		public int RESPONSE_ID { get; set; }
		public string RESPONSE_CONTENT { get; set; }
		public int RESPONSE_SCOREACHIEVED { get; set; }
		public bool RESPONSE_ISFINISHED { get; set; }
		public int RESPONSE_CONNTABLE_ID { get; set; }
		public int RESPONSE_STUDENTUSER_ID { get; set; }
	}
}