﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DATA;
namespace WebLayer.Models
{
	public class ClassViewModel
	{
		public List<USERModel> UserList { get; set; }
		public List<OsztalyModel> ClassList { get; set; }
		public String Class { get; set; }
	}
}