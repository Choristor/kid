﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace WebLayer.Models {
	public class UserViewModel {
		[Display(Name="Felhasználó név")]
		[DataType(DataType.Text)]
		[Required]
		public string azonosito { get; set; }

		[Display(Name ="Jelszó")]
		[DataType(DataType.Password)]
		[Required]
		public string password { get; set; }
	}
}