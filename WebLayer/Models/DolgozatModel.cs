﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLayer.Models
{
	public class DolgozatModel
	{
		public int EXERCISE_ID { get; set; }
		public string EXERCISE_NAME { get; set; }
		public DateTime EXERCISE_KITOLTESIIDO { get; set; }
		public int EXERCISE_SUBJECT_ID { get; set; }
		public int EXERCISE_UPLOADERUSER_ID { get; set; }
	}
}