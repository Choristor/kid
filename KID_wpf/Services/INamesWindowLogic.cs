﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KID_wpf
{
    public interface INamesWindowLogic
    {
        ObservableCollection<User> OsszesDiak(ClassD osztaly);
        ObservableCollection<ClassD> OsszesOsztaly();
        ObservableCollection<Question> KerdesLekerdezes(Test dolgozat);
        ObservableCollection<Answer> ValaszLekerdezes(Question QUESTION);
        ObservableCollection<User> KivalasztottDiak(ObservableCollection<User> osszesDiak);
        
        void TestCmd(Test dolgozat);
    }
}
