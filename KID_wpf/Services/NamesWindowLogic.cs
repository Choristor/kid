﻿using AutoMapper;
using BusinessLogic;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace KID_wpf
{
    class NamesWindowLogic : INamesWindowLogic
    {
        ILogic logic
        {
            get { return ServiceLocator.Current.GetInstance<ILogic>(); }
        }
        IMapper mapper
        {
            get { return ServiceLocator.Current.GetInstance<IMapper>(); }
        }
        INamesViewModel viewModel
        {
            get { return ServiceLocator.Current.GetInstance<INamesViewModel>(); }
        }
        public ObservableCollection<User> OsszesDiak(ClassD osztaly)
        {
            var osztalyDB = mapper.Map<ClassD, DATA.CLASS>(osztaly);
            try
            {
                var felhDB = logic.GetStudentsByClass(osztalyDB);
                return mapper.Map<IQueryable<DATA.USER>, ObservableCollection<User>>(felhDB);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return new ObservableCollection<User>();
        }

        public ObservableCollection<ClassD> OsszesOsztaly()
        {
            try
            {
                var osztalyDB = logic.ClassesGetAll();
                return mapper.Map<IQueryable<DATA.CLASS>, ObservableCollection<ClassD>>(osztalyDB);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return new ObservableCollection<ClassD>();
        }
        public ObservableCollection<Question> KerdesLekerdezes(Test dolgozat)
        {
            var dolgozatDB = mapper.Map<Test, DATA.EXERCISE>(dolgozat);
            try
            {
                 var QUESTIONDB = logic.QuestionsGetByExercise(dolgozatDB).ToList();
                return mapper.Map<List<DATA.QUESTION>, ObservableCollection<Question>>(QUESTIONDB);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return new ObservableCollection<Question>();
        }
        public ObservableCollection<Answer> ValaszLekerdezes(Question QUESTION)
        {
            var QUESTIONDB = mapper.Map<Question, DATA.QUESTION>(QUESTION);
            var valaszDB = logic.AnswersGetByQuestion(QUESTIONDB).ToList();
            return mapper.Map<List<DATA.ANSWER>, ObservableCollection<Answer>>(valaszDB);
        }

        public ObservableCollection<User> KivalasztottDiak(ObservableCollection<User> osszesDiak)
        {
            ObservableCollection<User> kivalasztottDiakok = new ObservableCollection<User>();
            foreach (var item in osszesDiak)
            {
                if (item.Checkd)
                {
                    kivalasztottDiakok.Add(item);
                }
            }
            return kivalasztottDiakok;
        }
        public void KivalasztottDiakokFeltoltese()
        {
            if (viewModel.Diakok.Count!=0)
            {
                foreach (var item in viewModel.Diakok)
                {
                    if (item.Checkd)
                    {
                        viewModel.KivalasztottDiakok.Add(item);
                    }
                } 
            }
            else
            {
                MessageBox.Show("Nem jelöltél ki diákot!");
            }
        }
        public void TestCmd(Test dolgozat)
        {
            KivalasztottDiakokFeltoltese();
            if (viewModel.KivalasztottDiakok.Count!=0)
            {
                for (int i = 0; i < viewModel.KivalasztottDiakok.Count; i++)
                {
                    for (int j = 0; j < viewModel.Kerdesek.Count; j++)
                    {
                        DATA.RESPONSE RESPONSE = new DATA.RESPONSE();
                        RESPONSE.RESPONSE_ISFINISHED = false;
                        RESPONSE.RESPONSE_STUDENTUSER_ID = 
                            viewModel.KivalasztottDiakok[i].USER_ID;
                        RESPONSE.RESPONSE_CONTENT = "";
                        logic.ResponseAdd(RESPONSE, 
                            viewModel.Kerdesek[j].QUESTION_ID, dolgozat.EXERCISE_ID);
                    }
                }
            }
            //User felh = mapper.Map<DATA.USER, User>(logic.UserGetById(2));
            //TestWindow win = new TestWindow(dolgozat, felh);
            //App.Current.Windows[0].Close();
            //win.ShowDialog();
            List<DATA.RESPONSE> res = logic.ResponsesGetAll().ToList();
            MessageBox.Show(res.Count.ToString());
        }
    }
}
