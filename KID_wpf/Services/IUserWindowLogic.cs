﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KID_wpf
{
    public interface IUserWindowLogic
    {
        void DolgozatLekerdezes();
        List<DATA.RESPONSE> BeirtValaszLekerdezes(User felhasznalo);
    }
}
