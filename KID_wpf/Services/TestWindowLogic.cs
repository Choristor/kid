﻿using AutoMapper;
using BusinessLogic;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace KID_wpf
{
    class TestWindowLogic:ITestWindowLogic
    {
        int i = 0;
        int pontszamok=0;
        int elertpontszam=0;
        ILogic logic
        {
            get { return ServiceLocator.Current.GetInstance<ILogic>(); }
        }
        IMapper mapper
        {
            get { return ServiceLocator.Current.GetInstance<IMapper>(); }
        }
        ITestViewModel viewModel
        {
            get { return ServiceLocator.Current.GetInstance<ITestViewModel>(); }
        }

        public ObservableCollection<Question> KerdesLekerdezes(Test dolgozat)
        {
            var dolgozatDB = mapper.Map<Test, DATA.EXERCISE>(dolgozat);
            try
            {
                var QUESTIONDB = logic.QuestionsGetByExercise(dolgozatDB);
                return mapper.Map<IQueryable<DATA.QUESTION>, ObservableCollection<Question>>(QUESTIONDB);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return new ObservableCollection<Question>();
        }

        public ObservableCollection<Answer> ValaszLekerdezes(Question QUESTION)
        {
            var QUESTIONDB = mapper.Map<Question, DATA.QUESTION>(QUESTION);
            try
            {
                var valaszDB = logic.AnswersGetByQuestion(QUESTIONDB);
                return mapper.Map<IQueryable<DATA.ANSWER>, ObservableCollection<Answer>>(valaszDB);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return new ObservableCollection<Answer>();
        }
        public DATA.RESPONSE beritValaszKivalasztas()
        {
            var dolgozatDB = mapper.Map<Test, DATA.EXERCISE>(viewModel.KivalasztottDolgozat);
            var QUESTIONDB = mapper.Map<Question, DATA.QUESTION>(viewModel.KivalasztottKerdes);
            List<DATA.RESPONSE> list=new List<DATA.RESPONSE>();
            try
            {
                list = logic.ResponseGetByExerciseAndQuestion(dolgozatDB, QUESTIONDB).ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
                foreach (var item in list)
                {
                    if(item.RESPONSE_STUDENTUSER_ID==viewModel.KivalasztottFelhasznalo.USER_ID)
                    {
                        if (!item.RESPONSE_ISFINISHED)
                        {
                            return item;
                        }
                    }
                }
            
            return null;
        }

        public void PontszamModositas()
        {
            DATA.RESPONSE RESPONSE = beritValaszKivalasztas();
            RESPONSE.RESPONSE_ISFINISHED = true;
            if (viewModel.KivalasztottKerdes.QUESTION_QUESTIONTYPE_ID == 1
                || viewModel.KivalasztottKerdes.QUESTION_QUESTIONTYPE_ID == 2)
            {
                foreach (var item in viewModel.Valaszok)
                {
                    if (item.Kivalasztotte)
                    {
                        item.Kivalasztotte = false;
                        RESPONSE.RESPONSE_CONTENT = item.ANSWER_CONTENT;
                    }
                }
            }
            if(viewModel.KivalasztottKerdes.QUESTION_QUESTIONTYPE_ID==3)
            {
                foreach (var item in viewModel.Valaszok)
                {
                    if (item.Kivalasztotte)
                    {
                        item.Kivalasztotte = false;
                        if (RESPONSE.RESPONSE_CONTENT == "")
                            RESPONSE.RESPONSE_CONTENT = item.ANSWER_CONTENT;
                        else
                            RESPONSE.RESPONSE_CONTENT += '\u263A' + item.ANSWER_CONTENT;
                    }
                }
            }
            if (viewModel.KivalasztottKerdes.QUESTION_QUESTIONTYPE_ID == 4)
            {
                RESPONSE.RESPONSE_CONTENT = viewModel.EsszeValasz;
            }
            if (viewModel.KivalasztottKerdes.QUESTION_QUESTIONTYPE_ID != 4)
            {
                if (logic.IsCorrect(RESPONSE) != 0)
                {
                    RESPONSE.RESPONSE_SCOREACHIEVED = logic.IsCorrect(RESPONSE);
                    elertpontszam += logic.IsCorrect(RESPONSE);
                }
                pontszamok += viewModel.KivalasztottKerdes.QUESTION_SCORE;
            }
            logic.ResponseModify(RESPONSE);
           
           
       
        }

        public void KovetkezoKerdes()
        {
            PontszamModositas();
            i++;
            viewModel.KerdesSzam = i+1 + "/" + viewModel.Kerdesek.Count + " kérdés";
                if (viewModel.Kerdesek.Count > i)
                {
                    viewModel.KivalasztottKerdes = viewModel.Kerdesek[i];
                    viewModel.Valaszok = ValaszLekerdezes(viewModel.KivalasztottKerdes);
                }
            
         }
        public void ElozoKerdes()
        {
            i--;
            viewModel.KerdesSzam = i + 1 + "/" + viewModel.Kerdesek.Count + " kérdés";
            viewModel.KivalasztottKerdes = viewModel.Kerdesek[i];
            viewModel.Valaszok = ValaszLekerdezes(viewModel.KivalasztottKerdes);
        }

        public void Befejezes()
        {
            PontszamModositas();
            var user = mapper.Map<User, DATA.USER>(viewModel.KivalasztottFelhasznalo);
            foreach (var item in logic.ResponsesGetByUser(user).ToList())
            {
                item.RESPONSE_ISFINISHED = true;
            }
            foreach (var item in viewModel.Kerdesek)
            {
                pontszamok += item.QUESTION_SCORE;
            }
            FinalWindow fn = new FinalWindow(elertpontszam, pontszamok);
            App.Current.Windows[0].Close();
            fn.ShowDialog();
        }
    }
}
