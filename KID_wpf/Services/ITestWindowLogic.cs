﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KID_wpf
{
    public interface ITestWindowLogic
    {
        ObservableCollection<Question> KerdesLekerdezes(Test dolgozat);
        ObservableCollection<Answer> ValaszLekerdezes(Question kerdes);
        void KovetkezoKerdes();
        void ElozoKerdes();
        void Befejezes();

    }
}
