﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KID_wpf
{
    interface IMainWindowLogic
    {
        ObservableCollection<Test> OsszesDolgozat(User felhasznalo);
        ObservableCollection<Subject> OsszesTantargy(ObservableCollection<Test> dolgozatok);
        ObservableCollection<Test> TantargyhozTartozoDolgozatok(Subject tantargy, ObservableCollection<Test> dolgozatok);
        ClassD OsztalyLekerdezes(int id);
        void NamesCmd(Test dolgozat);
    }
}
