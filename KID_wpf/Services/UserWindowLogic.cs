﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DATA;
using BusinessLogic;
using Microsoft.Practices.ServiceLocation;
using AutoMapper;
using System.Windows;

namespace KID_wpf
{
    class UserWindowLogic : IUserWindowLogic
    {
        ILogic logic
        {
            get { return ServiceLocator.Current.GetInstance<ILogic>(); }
        }
        IMapper mapper
        {
            get { return ServiceLocator.Current.GetInstance<IMapper>(); }
        }

        IUserViewModel viewModel
        {
            get { return ServiceLocator.Current.GetInstance<IUserViewModel>(); }
        }

        public List<RESPONSE> BeirtValaszLekerdezes(User USER)
        {
            var USERDB = mapper.Map<User, DATA.USER>(USER);
            try
            {
                List<RESPONSE> beirtvok = logic.ResponsesGetByUser(USERDB).ToList();
                return beirtvok;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return new List<RESPONSE>();
        }
        public void DolgozatLekerdezes()
        {
            List<DATA.RESPONSE> res = logic.ResponsesGetAll().ToList();
            MessageBox.Show(res.Count.ToString());
            try
            {
                viewModel.BeirtValaszok = BeirtValaszLekerdezes(viewModel.Felhasznalo);
            }
            catch { }
            foreach (var bvalasz in viewModel.BeirtValaszok)
            {
                Test doga=new Test();
                try
                {
                    var dolgozatDB = logic.ExerciseGetByResponse(bvalasz);
                    doga = mapper.Map<DATA.EXERCISE, Test>(dolgozatDB);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                if (doga.EXERCISE_ID!=0)
                {
                    if (viewModel.MegirandoDolgozatok == null)
                    {
                        viewModel.MegirandoDolgozatok = new System.Collections.ObjectModel.ObservableCollection<Test>();
                        viewModel.MegirandoDolgozatok.Add(doga);
                    }
                    bool vane = false;
                    foreach (var dolgozat in viewModel.MegirandoDolgozatok)
                    {
                        if (dolgozat.EXERCISE_ID == doga.EXERCISE_ID)
                        {
                            vane = true;
                        }
                    }
                    if (!vane)
                    {
                        viewModel.MegirandoDolgozatok.Add(doga);
                    } 
                }
            }
        }
    }
}
