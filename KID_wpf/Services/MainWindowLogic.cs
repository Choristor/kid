﻿using AutoMapper;
using BusinessLogic;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace KID_wpf
{
    class MainWindowLogic:IMainWindowLogic
    {
        ILogic logic
        {
            get { return ServiceLocator.Current.GetInstance<ILogic>(); }
        }
        IMapper mapper
        {
            get { return ServiceLocator.Current.GetInstance<IMapper>(); }
        }
        IMainViewModel viewModel
        {
            get { return ServiceLocator.Current.GetInstance<IMainViewModel>(); }
        }

        public void NamesCmd(Test dolgozat)
        {
            NamesWindow win = new NamesWindow(dolgozat, viewModel.Felhasznalo);
            App.Current.Windows[0].Close();
            win.ShowDialog();
        }


        public ObservableCollection<Test> OsszesDolgozat(User Felhasznalo)
        {
            var USERDB = mapper.Map<User, DATA.USER>(Felhasznalo);
            List<DATA.EXERCISE> dolgozatDB = new List<DATA.EXERCISE>();
            try
            {
                dolgozatDB = logic.ExercisesGetByUser(USERDB).ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return mapper.Map<List<DATA.EXERCISE>, ObservableCollection<Test>>(dolgozatDB);
        }
        public ObservableCollection<Subject> OsszesTantargy(ObservableCollection<Test> dolgozatok)
        {
            ObservableCollection<Subject> tantargyak = new ObservableCollection<Subject>();
            foreach (var doga in dolgozatok)
            {
                bool vane = false;
                DATA.SUBJECT tantargyDB = new DATA.SUBJECT();
                try
                {
                    tantargyDB = logic.SubjectGetById(doga.EXERCISE_SUBJECT_ID);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                foreach (var tantargy in tantargyak)
                {
                    if (doga.EXERCISE_SUBJECT_ID==tantargy.SUBJECT_ID)
                    {
                        vane = true;
                    }
                }
                if (!vane)
                {
                    tantargyak.Add(mapper.Map<DATA.SUBJECT, Subject>(
                            tantargyDB));
                }
            }
            return tantargyak;
        }

        public ClassD OsztalyLekerdezes(int id)
        {
            var classDB = logic.ClassGetById(id);
            try
            {
                return mapper.Map<DATA.CLASS, ClassD>(classDB);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return new ClassD();
        }

        public ObservableCollection<Test> TantargyhozTartozoDolgozatok(Subject tantargy, ObservableCollection<Test> dolgozatok)
        {
            ObservableCollection<Test> kiválasztottak=new ObservableCollection<Test>();
            if (dolgozatok.Count!=0)
            {
                foreach (var item in dolgozatok)
                {
                    if (item.EXERCISE_SUBJECT_ID == tantargy.SUBJECT_ID)
                    {
                        kiválasztottak.Add(item);
                    }
                } 
            }
            return kiválasztottak;
        }
    }
}
