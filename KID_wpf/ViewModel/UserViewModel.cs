﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DATA;
using GalaSoft.MvvmLight;

namespace KID_wpf
{
    class UserViewModel : ViewModelBase, IUserViewModel
    {
        List<DATA.RESPONSE> beirtValaszok;
        ObservableCollection<Test> megirandoDolgozatok;
        Test kivalasztottDolgozat;
        User felhasznalo;

        public List<RESPONSE> BeirtValaszok
        {
            get
            {
                return beirtValaszok;
            }

            set
            {
                Set(ref beirtValaszok, value);
            }
        }

        public ObservableCollection<Test> MegirandoDolgozatok
        {
            get
            {
                return megirandoDolgozatok;
            }

            set
            {
                Set(ref megirandoDolgozatok, value);
            }
        }

        public Test KivalasztottDolgozat
        {
            get
            {
                return kivalasztottDolgozat;
            }

            set
            {
                Set(ref kivalasztottDolgozat, value);
            }
        }

        public User Felhasznalo
        {
            get
            {
                return felhasznalo;
            }

            set
            {
                Set(ref felhasznalo, value);
            }
        }
    }
}
