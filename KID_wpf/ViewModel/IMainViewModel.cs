﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace KID_wpf
{
    public interface IMainViewModel
    {
        User Felhasznalo { get; set; }
        ClassD UserClass { get; set; }
        ObservableCollection<Test> Dolgozatok { get; set; }
        Test KiválasztottDolgozat { get; set; }
        ObservableCollection<Subject> Tantargyak { get; set; }
        ObservableCollection<Test> TantargyDolgozatok { get; set; }
        ICommand NamesCommand { get; }
    }
}
