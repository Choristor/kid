﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace KID_wpf
{
    class TestViewModel : ViewModelBase, ITestViewModel
    {
        ITestWindowLogic windowLogic
        {
            get { return ServiceLocator.Current.GetInstance<ITestWindowLogic>(); }
        }
        Test kivalasztottDolgozat;
        User kivalasztottFelhasznalo;
        ObservableCollection<Question> kerdesek;
        Question kivalasztottKerdes;
        ObservableCollection<Answer> valaszok;
        string kerdesSzam;
        string esszeValasz;
        string timeText;
        public Test KivalasztottDolgozat
        {
            get
            {
                return kivalasztottDolgozat;
            }

            set
            {
                Set(ref kivalasztottDolgozat, value);
            }
        }

        public ObservableCollection<Question> Kerdesek
        {
            get
            {
                return kerdesek;
            }

            set
            {
                Set(ref kerdesek, value);
            }
        }


        public Question KivalasztottKerdes
        {
            get
            {
                return kivalasztottKerdes;
            }

            set
            {
                Set(ref kivalasztottKerdes, value);
            }
        }

        public ObservableCollection<Answer> Valaszok
        {
            get
            {
                return valaszok;
            }

            set
            {
                Set(ref valaszok, value);
            }
        }

        public User KivalasztottFelhasznalo
        {
            get
            {
                return kivalasztottFelhasznalo;
            }

            set
            {
                Set(ref kivalasztottFelhasznalo, value);
            }
        }

        public string KerdesSzam
        {
            get
            {
                return kerdesSzam;
            }

            set
            {
                Set(ref kerdesSzam, value);
            }
        }

        public string EsszeValasz
        {
            get
            {
                return esszeValasz;
            }

            set
            {
                Set(ref esszeValasz, value);
            }
        }

        public string TimeText
        {
            get
            {
                return timeText;
            }

            set
            {
                Set(ref timeText, value);
            }
        }
    }
}
