﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace KID_wpf
{
    public interface IUserViewModel
    {
        List<DATA.RESPONSE> BeirtValaszok { get; set; }
        ObservableCollection<Test> MegirandoDolgozatok { get; set; }
        Test KivalasztottDolgozat { get; set; }
        User Felhasznalo { get; set; }
    }
}
