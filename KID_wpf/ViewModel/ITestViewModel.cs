﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace KID_wpf
{
    public interface ITestViewModel
    {
        Test KivalasztottDolgozat { get; set; }
        User KivalasztottFelhasznalo { get; set; }
        ObservableCollection<Question> Kerdesek { get; set; }
        Question KivalasztottKerdes { get; set; }
        ObservableCollection<Answer> Valaszok { get; set; }
        string KerdesSzam { get; set; }
        string EsszeValasz { get; set; }
        string TimeText { get; set; }
    }
}
