﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using Microsoft.Practices.ServiceLocation;
using GalaSoft.MvvmLight.CommandWpf;
using System.Windows;

namespace KID_wpf
{
    public class MainViewModel:ViewModelBase, IMainViewModel
    {
        IMainWindowLogic WindowLogic
        {
            get { return ServiceLocator.Current.GetInstance<IMainWindowLogic>(); }
        }

        AutoMapper.IMapper Mapper
        {
            get { return ServiceLocator.Current.GetInstance<AutoMapper.IMapper>(); }
        }
        ObservableCollection<Test> dolgozatok;
        Test kiválasztottDolgozat;
        User felhasznalo;
        ObservableCollection<Subject> tantargyak;
        ObservableCollection<Test> tantargyDolgozatok;
        ClassD userClass;

        public ObservableCollection<Test> Dolgozatok
        {
            get
            {
                return dolgozatok;
            }

            set
            {
                Set(ref dolgozatok, value);
            }
        }

        public Test KiválasztottDolgozat
        {
            get
            {
                return kiválasztottDolgozat;
            }

            set
            {
                Set(ref kiválasztottDolgozat, value);
            }
        }

        public ICommand NamesCommand { get; private set; }

        public User Felhasznalo
        {
            get
            {
                return felhasznalo;
            }

            set
            {
                Set(ref felhasznalo, value);
            }
        }

        public ObservableCollection<Subject> Tantargyak
        {
            get
            {
                return tantargyak;
            }

            set
            {
                Set(ref tantargyak, value);
            }
        }

        public ObservableCollection<Test> TantargyDolgozatok
        {
            get
            {
                return tantargyDolgozatok;
            }

            set
            {
                Set(ref tantargyDolgozatok, value);
            }
        }

        public ClassD UserClass
        {
            get
            {
                return userClass;
            }

            set
            {
                Set(ref userClass, value);
            }
        }

        public MainViewModel()
        {
            NamesCommand = new RelayCommand<Test>(WindowLogic.NamesCmd);
        }
    }
}
