﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace KID_wpf
{
    class NamesViewModel : ViewModelBase, INamesViewModel
    {
        INamesWindowLogic windowLogic
        {
            get { return ServiceLocator.Current.GetInstance<INamesWindowLogic>(); }
        }
        User kivalasztottFelhasznalo;
        ObservableCollection<User> diakok;
        ObservableCollection<User> kivalasztottDiakok;
        Test kivalasztottDolgozat;
        ObservableCollection<ClassD> osztalyok;
        ClassD kivalasztottOsztaly;
        ObservableCollection<Question> kerdesek;
        ObservableCollection<Answer> valaszok;
        public ICommand KezdesCommand { get; private set; }

        public ObservableCollection<User> Diakok
        {
            get
            {
                return diakok;
            }

            set
            {
                Set(ref diakok, value);
            }
        }

        public Test KivalasztottDolgozat
        {
            get
            {
                return kivalasztottDolgozat;
            }

            set
            {
                Set(ref kivalasztottDolgozat, value);
            }
        }

        public ObservableCollection<ClassD> Osztalyok
        {
            get
            {
                return osztalyok;
            }

            set
            {
                Set(ref osztalyok, value);
            }
        }

        public ClassD KivalasztottOsztaly
        {
            get
            {
                return kivalasztottOsztaly;
            }

            set
            {
                Set(ref kivalasztottOsztaly, value);
                Diakok = windowLogic.OsszesDiak(kivalasztottOsztaly);
            }
        }

        public ObservableCollection<User> KivalasztottDiakok
        {
            get
            {
                return kivalasztottDiakok;
            }

            set
            {
                Set(ref kivalasztottDiakok, value);
            }
        }

        public ObservableCollection<Question> Kerdesek
        {
            get
            {
                return kerdesek;
            }

            set
            {
                Set(ref kerdesek, value);
            }
        }

        public ObservableCollection<Answer> Valaszok
        {
            get
            {
                return valaszok;
            }

            set
            {
                Set(ref valaszok, value);
            }
        }

        public User KivalasztottFelhasznalo
        {
            get
            {
                return kivalasztottFelhasznalo;
            }

            set
            {
                Set(ref kivalasztottFelhasznalo, value);
            }
        }

        public NamesViewModel()
        {
            kivalasztottDiakok = new ObservableCollection<User>();
            osztalyok = windowLogic.OsszesOsztaly();
            diakok = new ObservableCollection<User>();
            KezdesCommand = new RelayCommand<Test>(windowLogic.TestCmd);
        }
    }
}
