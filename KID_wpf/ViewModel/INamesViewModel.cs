﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace KID_wpf
{
    public interface INamesViewModel
    {
        Test KivalasztottDolgozat { get; set; }
        User KivalasztottFelhasznalo { get; set; }
        ObservableCollection<User> Diakok { get; set; }
        ObservableCollection<ClassD> Osztalyok { get; set; }
        ObservableCollection<User> KivalasztottDiakok { get; set; }
        ObservableCollection<Question> Kerdesek { get; set; }
        ObservableCollection<Answer> Valaszok { get; set; }
        ClassD KivalasztottOsztaly { get; set; }
        ICommand KezdesCommand { get; }
    }
}
