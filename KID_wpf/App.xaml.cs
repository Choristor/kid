﻿using AutoMapper;
using BusinessLogic;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace KID_wpf
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<IMapper>(() => AutoMapperConfig.GetMapper());
            SimpleIoc.Default.Register<ILogic>(() => Logic.getInstance());
            SimpleIoc.Default.Register<IMainWindowLogic, MainWindowLogic>();
            SimpleIoc.Default.Register<IMainViewModel, MainViewModel>();
            SimpleIoc.Default.Register<INamesWindowLogic, NamesWindowLogic>();
            SimpleIoc.Default.Register<INamesViewModel, NamesViewModel>();
            SimpleIoc.Default.Register<ITestWindowLogic, TestWindowLogic>();
            SimpleIoc.Default.Register<ITestViewModel, TestViewModel>();
            SimpleIoc.Default.Register<IUserViewModel, UserViewModel>();
            SimpleIoc.Default.Register<IUserWindowLogic, UserWindowLogic>();

        }
    }
}
