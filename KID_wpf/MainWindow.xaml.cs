﻿using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KID_wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IMainViewModel viewModel
        {
            get { return ServiceLocator.Current.GetInstance<IMainViewModel>(); }
        }
        IMainWindowLogic windowLogic
        {
            get { return ServiceLocator.Current.GetInstance<IMainWindowLogic>(); }
        }
        public MainWindow(User Felhasznalo)
        {
            InitializeComponent();
            viewModel.Felhasznalo = Felhasznalo;
            viewModel.Dolgozatok = windowLogic.OsszesDolgozat(Felhasznalo);
            viewModel.Tantargyak = windowLogic.OsszesTantargy(viewModel.Dolgozatok);
            viewModel.UserClass = windowLogic.OsztalyLekerdezes(viewModel.Felhasznalo.USER_CLASS);
        }
        private void exitBtn_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Windows[0].Close();
        }

        private void minBtn_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void DragMove(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        private void AddColumn()
        {
            ColumnDefinition col = new ColumnDefinition() { Width = new GridLength(150) };
            tests.ColumnDefinitions.Add(col);
        }
        private Binding AddBinding(object source, string path)
        {
            Binding bb = new Binding();
            bb.Source = source;
            bb.Path = new PropertyPath(path);
            return bb;
        }
        private void Draw(Subject tantargy, int id)
        {
            AddColumn();
            Rectangle r = new Rectangle()
            {
                Width = 125,
                Height = 180,
                RadiusX = 9,
                RadiusY = 9,
                Stroke = Brushes.Black,
                StrokeThickness = 0.5
            };
            TextBlock tb = new TextBlock() { Margin = new Thickness(55, 20, 0, 0) };
            Binding bb = AddBinding(tantargy, "SUBJECT_NAME");
            tb.SetBinding(TextBlock.TextProperty, bb);
            ListBox lb = new ListBox()
            {
                BorderBrush = Brushes.Transparent,
                Background = Brushes.Transparent,
                Margin = new Thickness(0, 25, 0, 0),
                Height = 131,
                Width = 125
            };
            bb = new Binding();
            bb.Source = viewModel.TantargyDolgozatok;
            lb.SetBinding(ListBox.ItemsSourceProperty, bb);
            DataTemplate data = this.FindResource("LbData") as DataTemplate;
            lb.ItemTemplate = data;
            Grid.SetColumn(r, id);
            Grid.SetColumn(tb, id);
            Grid.SetColumn(lb, id);
            tests.Children.Add(r);
            tests.Children.Add(tb);
            tests.Children.Add(lb);
            lb.MouseDoubleClick += Lb_MouseDoubleClick;
            lb.SelectionChanged += Lb_SelectionChanged;
           
        }

        private void Lb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox lb = sender as ListBox;
            if (lb.SelectedItem!=null)
                viewModel.KiválasztottDolgozat = lb.SelectedItem as Test;
        }

        private void Lb_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            windowLogic.NamesCmd(viewModel.KiválasztottDolgozat);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < viewModel.Tantargyak.Count; i++)
            {
                foreach (var item in viewModel.Tantargyak)
                {
                    viewModel.TantargyDolgozatok = windowLogic.TantargyhozTartozoDolgozatok(item, viewModel.Dolgozatok);
                    Draw(viewModel.Tantargyak[i], i);

                }
            }
        }
    }
}
