﻿using AutoMapper;
using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KID_wpf
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void exitBtn_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Windows[0].Close();
        }

        private void minBtn_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void DragMove(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void login_Click(object sender, RoutedEventArgs e)
        {

            ILogic logic = Logic.getInstance();
            IMapper mapper = AutoMapperConfig.GetMapper();
            DATA.USER userDB=new DATA.USER();
            try
            {
                userDB = logic.Login(tbUser.Text, pbPass.Password);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            User Felhasznalo = mapper.Map<DATA.USER, User>(userDB);

            if (Felhasznalo.USER_AUTH == 2)
            {
                MainWindow mw = new MainWindow(Felhasznalo);
                App.Current.Windows[0].Close();
                mw.ShowDialog();
            }
            if (Felhasznalo.USER_AUTH == 3)
            {
                UserWindow uw = new UserWindow(Felhasznalo);
                App.Current.Windows[0].Close();
                uw.ShowDialog();
            }
        }
    }
}
