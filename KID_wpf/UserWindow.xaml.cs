﻿using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KID_wpf
{
    /// <summary>
    /// Interaction logic for UserWindow.xaml
    /// </summary>
    public partial class UserWindow : Window
    {
        IUserWindowLogic windowLogic
        {
            get { return ServiceLocator.Current.GetInstance<IUserWindowLogic>(); }
        }
        IUserViewModel viewModel
        {
            get { return ServiceLocator.Current.GetInstance<IUserViewModel>(); }
        }
        public UserWindow(User Felhasznalo)
        {
            InitializeComponent();
            viewModel.Felhasznalo = Felhasznalo;
            try
            {
                viewModel.BeirtValaszok = windowLogic.BeirtValaszLekerdezes(Felhasznalo);
            }
            catch { }
        }

        private void exitBtn_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Windows[0].Close();
        }

        private void minBtn_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void DragMove(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Update(object sender, RoutedEventArgs e)
        {
            windowLogic.DolgozatLekerdezes();
        }
        private void OpenWindow(object sender, MouseButtonEventArgs e)
        {
            TestWindow win = new TestWindow(viewModel.KivalasztottDolgozat, viewModel.Felhasznalo);
            App.Current.Windows[0].Close();
            win.ShowDialog();
        }
    }
}
