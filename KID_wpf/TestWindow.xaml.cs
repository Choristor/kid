﻿using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace KID_wpf
{
    /// <summary>
    /// Interaction logic for TestWindow.xaml
    /// </summary>
    public partial class TestWindow : Window
    {
        int min=2;
        int sec;
        public int Sec
        {
            get
            {
                return sec;
            }

            set
            {
                sec = value;
                if (sec < 10)
                    viewModel.TimeText = min + ":0" + sec;
                else
                    viewModel.TimeText = min + ":" + sec;
                
            }
        }
        DispatcherTimer timer;
        ITestWindowLogic windowLogic
        {
            get { return ServiceLocator.Current.GetInstance<ITestWindowLogic>(); }
        }
        ITestViewModel viewModel
        {
            get { return ServiceLocator.Current.GetInstance<ITestViewModel>(); }
        }

        public TestWindow(Test dolgozat, User Felhasznalo)
        {
            InitializeComponent();
            viewModel.KivalasztottDolgozat = dolgozat;
            viewModel.KivalasztottFelhasznalo = Felhasznalo;
            viewModel.Kerdesek = windowLogic.KerdesLekerdezes(dolgozat);
            if (viewModel.Kerdesek.Count != 0)
            {
                viewModel.KivalasztottKerdes = viewModel.Kerdesek[0];
            }
            else
            {
                viewModel.KivalasztottKerdes = new Question();
            }
            viewModel.Valaszok = windowLogic.ValaszLekerdezes(viewModel.KivalasztottKerdes);
            viewModel.KerdesSzam = "1/" + viewModel.Kerdesek.Count + " kérdés";
            viewModel.TimeText = min + ":0" + sec;
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (Sec==0)
            {
                min--;
                Sec = 60;
            }
            Sec--;
            if (min==0 && Sec==0)
            {
                timer.Stop();
                windowLogic.Befejezes();
            }
        }

        private void exitBtn_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Windows[0].Close();
        }

        private void minBtn_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void DragMove(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void QuestionList()
        {
            for (int i = 0; i < viewModel.Kerdesek.Count; i++)
            {
                Button b = new Button();
                b.Content = i+1;
                b.Background = Brushes.Transparent;
                b.BorderBrush = Brushes.Transparent;
                b.Width = 25;
                wpKerdesLista.Children.Add(b);
                b.Click += new RoutedEventHandler(Kerdesbtn_Click);
            }
        }

        private void Kerdesbtn_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            viewModel.KivalasztottKerdes = viewModel.Kerdesek[Convert.ToInt32(b.Content)];
            viewModel.Valaszok = windowLogic.ValaszLekerdezes(viewModel.KivalasztottKerdes);
            ShowAnswers();
        }

        private void AddRow()
        {
            RowDefinition row = new RowDefinition() { Height = new GridLength(25) };
            GValasz.RowDefinitions.Add(row);
        }

        private Binding ContentBinding(int id)
        {
            Binding bb = new Binding();
            bb.Source = viewModel.Valaszok[id];
            bb.Path = new PropertyPath("ANSWER_CONTENT");
            return bb;
        }

        private Binding CheckedBinding(int id)
        {
            Binding bb2 = new Binding();
            bb2.Source = viewModel.Valaszok[id];
            bb2.Path = new PropertyPath("Kivalasztotte");
            return bb2;
        }
        
        private void AddRadioButton(int id)
        {
            Binding bb = ContentBinding(id);
            Binding bb2 = CheckedBinding(id);
            RadioButton rButton = new RadioButton();
            Grid.SetRow(rButton, id);
            rButton.SetBinding(RadioButton.ContentProperty, bb);
            rButton.SetBinding(RadioButton.IsCheckedProperty, bb2);
            rButton.Margin = new Thickness(10, 10, 0, 0);
            GValasz.Children.Add(rButton);
        }

        private void AddCheckBox(int id)
        {
            Binding bb = ContentBinding(id);
            Binding bb2 = CheckedBinding(id);
            CheckBox cb = new CheckBox();
            Grid.SetRow(cb, id);
            cb.SetBinding(CheckBox.ContentProperty, bb);
            cb.SetBinding(CheckBox.IsCheckedProperty, bb2);
            cb.Margin = new Thickness(10, 10, 0, 0);
            GValasz.Children.Add(cb);
        }

        private void AddTextBox()
        {
            AddRow();
            TextBox tb = new TextBox();
            Grid.SetRow(tb, 0);
            Binding bb = new Binding("Value");
            bb.Source = viewModel.EsszeValasz;
            GValasz.Children.Add(tb);
            
            
        }

        private void ShowAnswers()
        {
            if (viewModel.KivalasztottKerdes.QUESTION_QUESTIONTYPE_ID == 1)
            {
                GValasz.Children.Clear();
                AddRow();
                AddRow();
                if (viewModel.Valaszok.Count!=0)
                {
                    AddRadioButton(0);
                    AddRadioButton(1); 
                }
            }
            if (viewModel.KivalasztottKerdes.QUESTION_QUESTIONTYPE_ID == 2)
            {
                GValasz.Children.Clear();
                AddRow();
                AddRow();
                AddRow();
                AddRow();
                if (viewModel.Valaszok.Count!=0)
                {
                    AddRadioButton(0);
                    AddRadioButton(1);
                    AddRadioButton(2);
                    AddRadioButton(3); 
                }

            }
            if (viewModel.KivalasztottKerdes.QUESTION_QUESTIONTYPE_ID == 3)
            {
                GValasz.Children.Clear();
                AddRow();
                AddRow();
                AddRow();
                AddRow();
                if (viewModel.Valaszok.Count!=0)
                {
                    AddCheckBox(0);
                    AddCheckBox(1);
                    AddCheckBox(2);
                    AddCheckBox(3); 
                }
            }
            if (viewModel.KivalasztottKerdes.QUESTION_QUESTIONTYPE_ID == 4 ||
                viewModel.KivalasztottKerdes.QUESTION_QUESTIONTYPE_ID == 5)
            {
                GValasz.Children.Clear();
                AddTextBox();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            QuestionList();
            ShowAnswers();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            windowLogic.KovetkezoKerdes();
            ShowAnswers();
        }

        private void button_Click_1(object sender, RoutedEventArgs e)
        {
            windowLogic.ElozoKerdes();
            ShowAnswers();
        }

        private void button_Copy_Click(object sender, RoutedEventArgs e)
        {
            windowLogic.Befejezes();
        }
    }
}
