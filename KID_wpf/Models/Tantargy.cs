﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KID_wpf
{
    public class Subject
    {
        int tantargyId;
        string osztalyNev;

        public int SUBJECT_ID
        {
            get
            {
                return tantargyId;
            }

            set
            {
                tantargyId = value;
            }
        }

        public string SUBJECT_NAME
        {
            get
            {
                return osztalyNev;
            }

            set
            {
                osztalyNev = value;
            }
        }
    }
}
