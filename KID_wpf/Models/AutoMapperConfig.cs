﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KID_wpf
{
    static class AutoMapperConfig
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<DATA.EXERCISE, Test>().ReverseMap();
                    cfg.CreateMap<DATA.USER, User>().ReverseMap();
                    cfg.CreateMap<DATA.QUESTION, Question>().ReverseMap();
                    cfg.CreateMap<DATA.ANSWER, Answer>().ReverseMap();
                    cfg.CreateMap<DATA.CLASS, ClassD>().ReverseMap();
                    cfg.CreateMap<DATA.SUBJECT, Subject>().ReverseMap();
                    cfg.CreateMap<DATA.QUESTIONTYPE, QuestionType>().ReverseMap();
                });
                
            return config.CreateMapper();
        }
    }
}
