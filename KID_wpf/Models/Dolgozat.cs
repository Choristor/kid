﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KID_wpf
{
    public class Test : ObservableObject
    {
        int dolgozatId;
        string dolgozatAzon;
        int tantargyId;
        int ido;
        int feltoltoFelhId;

        public int EXERCISE_ID
        {
            get
            {
                return dolgozatId;
            }

            set
            {
                Set(ref dolgozatId, value);
            }
        }

        public string EXERCISE_NAME
        {
            get
            {
                return dolgozatAzon;
            }

            set
            {
                Set(ref dolgozatAzon, value);
            }
        }

        public int EXERCISE_SUBJECT_ID
        {
            get
            {
                return tantargyId;
            }

            set
            {
                Set(ref tantargyId, value);
            }
        }

        public int EXERCISE_UPLOADERUSER_ID
        {
            get
            {
                return feltoltoFelhId;
            }

            set
            {
                Set(ref feltoltoFelhId, value);
            }
        }

        public int EXERCISE_TIME
        {
            get
            {
                return ido;
            }

            set
            {
                ido = value;
            }
        }
    }
}
