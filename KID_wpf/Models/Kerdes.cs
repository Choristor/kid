﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KID_wpf
{
    public class Question : ObservableObject
    {
        int kerdeId;
        string nev;
        string QUESTIONTartalom;
        int QUESTIONTipus;
        int pontszam;
        int tantargyId;
        int feltoltoFelhId;
        bool kivalasztotte;

        public int QUESTION_ID
        {
            get
            {
                return kerdeId;
            }

            set
            {
                Set(ref kerdeId, value);
            }
        }

        public string QUESTION_CONTENT
        {
            get
            {
                return QUESTIONTartalom;
            }

            set
            {
                Set(ref QUESTIONTartalom, value);
            }
        }

        public int QUESTION_QUESTIONTYPE_ID
        {
            get
            {
                return QUESTIONTipus;
            }

            set
            {
                Set(ref QUESTIONTipus, value);
            }
        }

        public int QUESTION_SUBJECT_ID
        {
            get
            {
                return tantargyId;
            }

            set
            {
                Set(ref tantargyId, value);
            }
        }

        public int QUESTION_UPLOADERUSER_ID
        {
            get
            {
                return feltoltoFelhId;
            }

            set
            {
                Set(ref feltoltoFelhId, value);
            }
        }

        public int QUESTION_SCORE
        {
            get
            {
                return pontszam;
            }

            set
            {
                Set(ref pontszam, value);
            }
        }

        public bool Kivalasztotte
        {
            get
            {
                return kivalasztotte;
            }

            set
            {
                Set(ref kivalasztotte, value);
            }
        }

        public string QUESTION_NAME
        {
            get
            {
                return nev;
            }

            set
            {
                nev = value;
            }
        }
    }
}
