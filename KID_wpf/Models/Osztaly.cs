﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KID_wpf
{
    public class ClassD
    {
        int osztalyId;
        string osztalyNev;

        public string CLASS_NAME
        {
            get
            {
                return osztalyNev;
            }

            set
            {
                osztalyNev = value;
            }
        }

        public int CLASS_ID
        {
            get
            {
                return osztalyId;
            }

            set
            {
                osztalyId = value;
            }
        }
    }
}
