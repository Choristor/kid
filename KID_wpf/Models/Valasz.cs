﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KID_wpf
{
    public class Answer : ObservableObject
    {
        int valaszId;
        string valaszTartalom;
        bool helyese;
        int QUESTIONId;
        bool kivalasztotte;

        public int ANSWER_ID
        {
            get
            {
                return valaszId;
            }

            set
            {
                Set(ref valaszId, value);
            }
        }

        public string ANSWER_CONTENT
        {
            get
            {
                return valaszTartalom;
            }

            set
            {
                Set(ref valaszTartalom, value);
            }
        }

        public bool ANSWER_ISCORRECT
        {
            get
            {
                return helyese;
            }

            set
            {
                Set(ref helyese, value);
            }
        }

        public int ANSWER_QUESTION_ID
        {
            get
            {
                return QUESTIONId;
            }

            set
            {
                Set(ref QUESTIONId, value);
            }
        }

        public bool Kivalasztotte
        {
            get
            {
                return kivalasztotte;
            }

            set
            {
                Set(ref kivalasztotte, value);
            }
        }
    }
}
