﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KID_wpf
{
    public class QuestionType
    {
        int id;
        string name;

        public string QUESTIONTYPE_NAME
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public int QUESTIONTYPE_ID
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
    }
}
