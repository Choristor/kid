﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KID_wpf
{
    public class User : ObservableObject
    {
        int felhasznaolId;
        string felhasznaolNev;
        string jelszo;
        int osztaly;
        DateTime SzulNap;
        int jogosultsag;
        bool checkd;

        public int USER_ID
        {
            get
            {
                return felhasznaolId;
            }

            set
            {
                Set(ref felhasznaolId, value);
            }
        }

        public string USER_NAME
        {
            get
            {
                return felhasznaolNev;
            }

            set
            {
                Set(ref felhasznaolNev, value);
            }
        }

        public string USER_PASSWORD
        {
            get
            {
                return jelszo;
            }

            set
            {
                Set(ref jelszo, value);
            }
        }

        public int USER_CLASS
        {
            get
            {
                return osztaly;
            }

            set
            {
                Set(ref osztaly, value);
            }
        }

        public DateTime USER_BDATE
        {
            get
            {
                return SzulNap;
            }

            set
            {
                Set(ref SzulNap, value);
            }
        }

        public int USER_AUTH
        {
            get
            {
                return jogosultsag;
            }

            set
            {
                Set(ref jogosultsag, value);
            }
        }

        public bool Checkd
        {
            get
            {
                return checkd;
            }

            set
            {
                Set(ref checkd, value);
            }
        }
    }
}
