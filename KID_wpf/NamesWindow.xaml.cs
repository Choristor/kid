﻿using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KID_wpf
{
    /// <summary>
    /// Interaction logic for NamesWindow.xaml
    /// </summary>
    public partial class NamesWindow : Window
    {
        INamesViewModel viewModel
        {
            get { return ServiceLocator.Current.GetInstance<INamesViewModel>(); }
        }
        INamesWindowLogic windowLogic
        {
            get { return ServiceLocator.Current.GetInstance<INamesWindowLogic>(); }
        }
        private void exitBtn_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Windows[0].Close();
        }

        private void minBtn_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void DragMove(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        public NamesWindow(Test dolgozat, User Felhasznalo)
        {
            InitializeComponent();
            viewModel.KivalasztottDolgozat = dolgozat;
            viewModel.KivalasztottFelhasznalo = Felhasznalo;
            viewModel.Kerdesek = windowLogic.KerdesLekerdezes(dolgozat);
        }
    }
}
