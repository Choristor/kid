﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DATA;

namespace BusinessLogic
{    
    public class FakeLogic : ILogic
    {
        private static ILogic singleton = null;
        List<USER> USERk = new List<USER>() { new USER() };
        List<QUESTION> QUESTIONek = new List<QUESTION>() { new QUESTION() };
        List<ANSWER> valaszok = new List<ANSWER>() { new ANSWER() };
        List<EXERCISE> dolgozatok = new List<EXERCISE>() { new EXERCISE() };
        List<SUBJECT> tantargyak = new List<SUBJECT>() { new SUBJECT() };
        List<RESPONSE> RESPONSEok = new List<RESPONSE>() { new RESPONSE() };
        List<CLASS> osztalyok = new List<CLASS>() { new CLASS() };
        List<AUTHORITY> jogosultsagok = new List<AUTHORITY>() { new AUTHORITY() };
        List<QUESTIONTYPE> QUESTIONtipusok = new List<QUESTIONTYPE>() { new QUESTIONTYPE() };
        List<CONNTABLE_QUESTION_EXERCISE> connTable = new List<CONNTABLE_QUESTION_EXERCISE>() { new CONNTABLE_QUESTION_EXERCISE() };


        public static ILogic getInstance()
        {
            if (singleton == null)
                singleton = new FakeLogic();
            return singleton;
        }

        private FakeLogic()
        {
            tantargyak.Add(new SUBJECT() { SUBJECT_ID = 1, SUBJECT_NAME = "űrkutatás" });

            tantargyak.Add(new SUBJECT() { SUBJECT_ID = 2, SUBJECT_NAME = "sörfőzés" });

            jogosultsagok.Add(new AUTHORITY() { AUTHORITY_ID = 1, AUTHORITY_NAME = "admin" });
            jogosultsagok.Add(new AUTHORITY() { AUTHORITY_ID = 2, AUTHORITY_NAME = "tanár" });
            jogosultsagok.Add(new AUTHORITY() { AUTHORITY_ID = 3, AUTHORITY_NAME = "diák" });

            osztalyok.Add(new CLASS() { CLASS_ID = 1, CLASS_NAME = "2/a" });
            osztalyok.Add(new CLASS() { CLASS_ID = 2, CLASS_NAME = "3/a" });

            QUESTIONtipusok.Add(new QUESTIONTYPE() { QUESTIONTYPE_ID = 1, QUESTIONTYPE_NAME = "Igaz-hamis" });
            QUESTIONtipusok.Add(new QUESTIONTYPE() { QUESTIONTYPE_ID = 2, QUESTIONTYPE_NAME = "ABCD" });
            QUESTIONtipusok.Add(new QUESTIONTYPE() { QUESTIONTYPE_ID = 3, QUESTIONTYPE_NAME = "ABCD-több jó" });

            connTable.Add(new CONNTABLE_QUESTION_EXERCISE() {CONNTABLE_QUESTION_EXERCISE_ID=1, CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID = 1, CONNTABLE_QUESTION_EXERCISE_QUESTION_ID=1 });
            connTable.Add(new CONNTABLE_QUESTION_EXERCISE() { CONNTABLE_QUESTION_EXERCISE_ID = 2, CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID = 1, CONNTABLE_QUESTION_EXERCISE_QUESTION_ID = 2 });
            connTable.Add(new CONNTABLE_QUESTION_EXERCISE() { CONNTABLE_QUESTION_EXERCISE_ID = 3, CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID = 2, CONNTABLE_QUESTION_EXERCISE_QUESTION_ID=3});

            RESPONSEok.Add(new RESPONSE()
            {
                RESPONSE_ID = 1,
                RESPONSE_CONTENT="valami",
                RESPONSE_SCOREACHIEVED=0,
                RESPONSE_STUDENTUSER_ID=2,
                RESPONSE_ISFINISHED=true,
                RESPONSE_CONNTABLE_ID=1
            });
            
            RESPONSEok.Add(new RESPONSE()
            {
                RESPONSE_ID = 2,
                RESPONSE_CONTENT = "valami2",
                RESPONSE_SCOREACHIEVED = 1,
                RESPONSE_STUDENTUSER_ID = 2,
                RESPONSE_ISFINISHED = true,
                RESPONSE_CONNTABLE_ID = 2
            });

            RESPONSEok.Add(new RESPONSE()
            {
                RESPONSE_ID = 3,
                RESPONSE_ISFINISHED = false,
                RESPONSE_CONNTABLE_ID = 3
            });


            dolgozatok.Add(new EXERCISE()
            {
                EXERCISE_ID = 1,
                EXERCISE_NAME = "tesztdoga",
                EXERCISE_UPLOADERUSER_ID = 1,
                EXERCISE_SUBJECT_ID = 1,
            });

            dolgozatok.Add(new EXERCISE()
            {
                EXERCISE_ID = 2,
                EXERCISE_NAME = "tesztdoga2",
                EXERCISE_UPLOADERUSER_ID = 1,
                EXERCISE_SUBJECT_ID = 2,
            });

            USERk.Add(new USER()
            {
                USER_ID = 1,
                USER_NAME = "Bela",
                USER_PASSWORD = "Bela",
                USER_AUTH = 1,
                USER_CLASS = 1,
                USER_BDATE = DateTime.Now
            });
            USERk.Add(new USER()
            {
                USER_ID = 2,
                USER_NAME = "Diak1",
                USER_PASSWORD = "Bela",
                USER_AUTH = 2,
                USER_CLASS = 2,
                USER_BDATE = DateTime.Now
            });

            USERk.Add(new USER()
            {
                USER_ID = 3,
                USER_NAME = "Mr.Admin",
                USER_PASSWORD = "Bela",
                USER_AUTH = 1,
                USER_CLASS = null,
                USER_BDATE = DateTime.Now
            });

            QUESTIONek.Add(new QUESTION()
            {
                QUESTION_ID = 1,
                QUESTION_UPLOADERUSER_ID = 0,
                QUESTION_CONTENT = "Ez az állítás hamis.",
                QUESTION_QUESTIONTYPE_ID = 1,
                QUESTION_SUBJECT_ID = 1,
                QUESTION_SCORE = 1,
            });
            QUESTIONek.Add(new QUESTION()
            {
                QUESTION_ID = 2,
                QUESTION_UPLOADERUSER_ID = 0,
                QUESTION_SCORE = 1,
                QUESTION_SUBJECT_ID = 1,
                QUESTION_CONTENT = "Mennyi az annyi?",
                QUESTION_QUESTIONTYPE_ID = 2
            });
            QUESTIONek.Add(new QUESTION()
            {
                QUESTION_ID = 3,
                QUESTION_UPLOADERUSER_ID = 0,
                QUESTION_SCORE = 4,
                QUESTION_SUBJECT_ID = 2,
                QUESTION_CONTENT = "Csak az első, a második, a legutolsó és az utolsó előtti a helyes válasz.",
                QUESTION_QUESTIONTYPE_ID = 3
            });



            valaszok.Add(new ANSWER()
            {
                ANSWER_ID = 1,
                ANSWER_ISCORRECT = true,
                ANSWER_CONTENT = "IGEN",
                ANSWER_QUESTION_ID = 1,
            });

            valaszok.Add(new ANSWER()
            {
                ANSWER_ID = 2,
                ANSWER_ISCORRECT = false,
                ANSWER_CONTENT = "NEM",
                ANSWER_QUESTION_ID = 1,
            });

            valaszok.Add(new ANSWER()
            {
                ANSWER_ID = 3,
                ANSWER_ISCORRECT = true,
                ANSWER_CONTENT = "42",
                ANSWER_QUESTION_ID = 2,
            });

            valaszok.Add(new ANSWER()
            {
                ANSWER_ID = 4,
                ANSWER_ISCORRECT = false,
                ANSWER_CONTENT = "43",
                ANSWER_QUESTION_ID = 2,
            });

            valaszok.Add(new ANSWER()
            {
                ANSWER_ID = 5,
                ANSWER_ISCORRECT = false,
                ANSWER_CONTENT = "NEM",
                ANSWER_QUESTION_ID = 2,
            });

            valaszok.Add(new ANSWER()
            {
                ANSWER_ID = 6,
                ANSWER_ISCORRECT = false,
                ANSWER_CONTENT = "SÖR",
                ANSWER_QUESTION_ID = 2,
            });

            valaszok.Add(new ANSWER()
            {
                ANSWER_ID = 7,
                ANSWER_ISCORRECT = true,
                ANSWER_CONTENT = "ELSŐ",
                ANSWER_QUESTION_ID = 3,
            });

            valaszok.Add(new ANSWER()
            {
                ANSWER_ID = 8,
                ANSWER_ISCORRECT = true,
                ANSWER_CONTENT = "MÁSODIK",
                ANSWER_QUESTION_ID = 3,
            });

            valaszok.Add(new ANSWER()
            {
                ANSWER_ID = 9,
                ANSWER_ISCORRECT = true,
                ANSWER_CONTENT = "HARMADIK",
                ANSWER_QUESTION_ID = 3,
            });

            valaszok.Add(new ANSWER()
            {
                ANSWER_ID = 10,
                ANSWER_ISCORRECT = true,
                ANSWER_CONTENT = "NEGYEDIK",
                ANSWER_QUESTION_ID = 3,
            });


        }

        

        public USER Login(string USERNev, string jelszo)
        {
            foreach (USER f in USERk)
            {
                if (f.USER_NAME == USERNev && f.USER_PASSWORD == jelszo)
                    return f;
            }
            throw new ArgumentException("Nincs ilyen felhasználó xxxx");
        }

        public EXERCISE ExerciseGetByName(string azonosito)
        {
            foreach (EXERCISE d in dolgozatok)
            {
                if (d.EXERCISE_NAME == azonosito)
                    return d;
            }
            throw new ArgumentException("Nincs ilyen azonosítójú dolgozat: "+azonosito);
        }

        public void ExerciseDelete(EXERCISE torlendoDolgozat)
        {
            EXERCISE fe = null; ;
            foreach (EXERCISE f in dolgozatok)
            {
                if (f.EXERCISE_ID == torlendoDolgozat.EXERCISE_ID)
                    fe = f;
            }
            if (fe != null)
                dolgozatok.Remove(fe);
            else
                throw new ArgumentException("NINCS ILYEN dolgozat");
        }





        public int IsCorrect(RESPONSE RESPONSE)
        {
            int k1 = (int)RESPONSE.CONNTABLE_QUESTION_EXERCISE.CONNTABLE_QUESTION_EXERCISE_QUESTION_ID;
            QUESTION k = null;
            foreach (var item in QUESTIONek)
            {
                if (item.QUESTION_ID == k1)
                    k = item;
            }
            List<ANSWER> v = new List<ANSWER>();
            foreach (var item in valaszok)
            {
                if (item.ANSWER_QUESTION_ID == k.QUESTION_ID)
                    v.Add(item);
            }
            foreach (ANSWER item in valaszok)
            {
                if (item.ANSWER_CONTENT == RESPONSE.RESPONSE_CONTENT)
                    return 1;
            }
            throw new ArgumentException("ÉRVÉNYTELEN VÁLASZ");
        }



        public IQueryable<SUBJECT> SubjectsGetAll()
        {
            return tantargyak.AsQueryable();
        }


        public IQueryable<QUESTION> GetRandomQuestions(int hanyDarabot, SUBJECT melyikTargybol)
        {
            List<QUESTION> r = new List<QUESTION>();
            int i = 0;
            while (i < hanyDarabot && i < QUESTIONek.Count)
			{
                r.Add(QUESTIONek[i]);
				i++;
			}
            return r.AsQueryable();
        }


        public USER UserGetById(int USER_id)
        {
            USER f = null;
            foreach (USER f1 in USERk)
            {
                if (f1.USER_ID == USER_id)
                    f = f1;
            }
            if (f != null)
                return f;
            throw new ArgumentException("NINCS ILYEN ID-JŰ FELHASZNÁLÓ");
        }

        public void UserAdd(USER USER)
        {
            USER.USER_ID = USERk.Count;
            USERk.Add(USER);
        }


        
        public void UserModify(USER USER)
        {
            bool l = false;
            foreach (USER f in USERk)
            {
                if (f.USER_ID == USER.USER_ID)
                {
                    f.USER_NAME = USER.USER_NAME;
                    f.USER_PASSWORD = USER.USER_PASSWORD;
                    f.USER_CLASS = USER.USER_CLASS;
                    f.USER_BDATE = USER.USER_BDATE;
                    f.USER_AUTH = USER.USER_AUTH;
                    l = true;
                }
            }
            if (!l)
                throw new ArgumentException("NINCS ILYEN FELHASZNÁLÓ");
        }

        public void UserDelete(USER USER)
        {
            USER fe = null; ;
            foreach (USER f in USERk)
            {
                if (f.USER_ID == USER.USER_ID)
                    fe = f;
            }
            if (fe != null)
                USERk.Remove(fe);
            else
                throw new ArgumentException("NINCS ILYEN FELHASZNÁLÓ");
        }


        public void SubjectAdd(SUBJECT tantargy)
        {
            tantargy.SUBJECT_ID = tantargyak.Count;
            tantargyak.Add(tantargy);
        }


        public void SubjectModify(SUBJECT tantargy)
        {
            bool l = false;
            foreach (SUBJECT t in tantargyak)
            {
                if (tantargy.SUBJECT_ID == t.SUBJECT_ID)
                {
                    t.SUBJECT_NAME = tantargy.SUBJECT_NAME;
                    l = true;
                }
            }
            if (!l)
                throw new ArgumentException("NINCS ILYEN TANTÁRGY");
        }

        public void SubjectDelete(SUBJECT tantargy)
        {
            SUBJECT d = null;
            foreach (SUBJECT t in tantargyak)
            {
                if (t.SUBJECT_ID == tantargy.SUBJECT_ID)
                    d = t;
            }
            if (d != null)
                tantargyak.Remove(d);
            throw new ArgumentException("Nincs ilyen tantárgy");
        }

        public IQueryable<EXERCISE> ExercisesGetByUser(USER USER)
        {
            List<EXERCISE> l = new List<EXERCISE>();
            foreach (EXERCISE d in dolgozatok)
            {
                if (d.EXERCISE_UPLOADERUSER_ID == USER.USER_ID)
                    l.Add(d);
            }
            if (l.Count > 0)
                return l.AsQueryable();
            throw new ArgumentException(USER.USER_ID + " IDJŰ FELHASZNÁLÓHOZ NEM TARTOZIK EXERCISE");
        }

        public IQueryable<USER> GetStudentsByClass(CLASS melyikOsztaly)
        {
            List<USER> r = new List<USER>();
            r.Clear();
            foreach (USER f in USERk)
            {
                if (f.USER_AUTH == 2 && f.USER_CLASS == melyikOsztaly.CLASS_ID)
                    r.Add(f);
            }
            return r.AsQueryable();
        }

        public IQueryable<QUESTION> QuestionsGetByExercise(EXERCISE dolgozat)
        {
            if (dolgozat.EXERCISE_ID == 0) return new List<QUESTION>() { QUESTIONek[0] }.AsQueryable();
            if (dolgozat.EXERCISE_ID > 0)
            {

                if (dolgozat.EXERCISE_ID % 2 == 0) return new List<QUESTION>() { QUESTIONek[0], QUESTIONek[1] }.AsQueryable();
                else
                    return new List<QUESTION>() { QUESTIONek[1], QUESTIONek[2] }.AsQueryable();
            }
            throw new ArgumentException("NO DATA");
        }

        public IQueryable<ANSWER> AnswersGetByQuestion(QUESTION whichQuestion)
        {
            List<ANSWER> r = new List<ANSWER>();
            foreach (ANSWER v in valaszok)
            {
                if (v.ANSWER_QUESTION_ID == whichQuestion.QUESTION_ID)
                    r.Add(v);
            }
            if (r.Count > 0)
                return r.AsQueryable();
            throw new ArgumentException("NO DATA");
        }

        public void ResponseAdd(RESPONSE RESPONSE, int QUESTIONID, int dolgozatID)
        {
            RESPONSE.RESPONSE_ID = RESPONSEok.Count;
            
            CONNTABLE_QUESTION_EXERCISE c = new CONNTABLE_QUESTION_EXERCISE()
            {
                CONNTABLE_QUESTION_EXERCISE_ID = connTable.Count,
                CONNTABLE_QUESTION_EXERCISE_QUESTION_ID = QUESTIONID,
                CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID = dolgozatID,
            };
            RESPONSE.RESPONSE_CONNTABLE_ID = c.CONNTABLE_QUESTION_EXERCISE_ID;
            RESPONSE.CONNTABLE_QUESTION_EXERCISE = c;
            RESPONSEok.Add(RESPONSE);
            connTable.Add(c);
        }

        public IQueryable<RESPONSE> ResponsesGetByUser(USER USER)
        {
            List<RESPONSE> l = new List<RESPONSE>();
            foreach (RESPONSE b in RESPONSEok)
            {
                if (b.RESPONSE_STUDENTUSER_ID == USER.USER_ID)
                    l.Add(b);
            }
            if(l.Count>0)
                return l.AsQueryable();
            throw new ArgumentException("NO DATA");
        }

        public void ResponseModify(RESPONSE RESPONSE)
        {
            foreach (RESPONSE b in RESPONSEok)
            {
                if (b.RESPONSE_ID == RESPONSE.RESPONSE_ID)
                {
                    b.RESPONSE_CONTENT = RESPONSE.RESPONSE_CONTENT;
                    b.RESPONSE_SCOREACHIEVED = RESPONSE.RESPONSE_SCOREACHIEVED;
                    b.RESPONSE_ISFINISHED = RESPONSE.RESPONSE_ISFINISHED;
                }
            }
        }


        public void AuthorityAdd(AUTHORITY jogosultsag)
        {
            jogosultsag.AUTHORITY_ID = jogosultsagok.Count;
            jogosultsagok.Add(jogosultsag);
        }

        public void AuthorityModify(AUTHORITY jogosultsag)
        {
            foreach (AUTHORITY j in jogosultsagok)
            {
                if(j.AUTHORITY_ID == jogosultsag.AUTHORITY_ID)
                {
                    j.AUTHORITY_NAME = jogosultsag.AUTHORITY_NAME;
                }
            }
        }

        public void AuthorityDelete(AUTHORITY jogosultsag)
        {
            AUTHORITY jog = null;
            foreach (AUTHORITY j in jogosultsagok)
            {
                if (j.AUTHORITY_ID == jogosultsag.AUTHORITY_ID)
                {
                    jog = j;
                }
            }
            if (jog != null)
                jogosultsagok.Remove(jog);
            throw new ArgumentException("NO DATA");
        }

        public void QuestionTypeAdd(QUESTIONTYPE QUESTIONtipus)
        {
            QUESTIONtipus.QUESTIONTYPE_ID = jogosultsagok.Count;
            QUESTIONtipusok.Add(QUESTIONtipus);
        }

        public void QuestionTypeModify(QUESTIONTYPE QUESTIONtipus)
        {
            foreach (QUESTIONTYPE j in QUESTIONtipusok)
            {
                if (j.QUESTIONTYPE_ID == QUESTIONtipus.QUESTIONTYPE_ID)
                {
                    j.QUESTIONTYPE_NAME = QUESTIONtipus.QUESTIONTYPE_NAME;
                }
            }
        }

        public void QuestionTypeDelete(QUESTIONTYPE QUESTIONtipus)
        {
            QUESTIONTYPE jog = null;
            foreach (QUESTIONTYPE j in QUESTIONtipusok)
            {
                if (j.QUESTIONTYPE_ID == QUESTIONtipus.QUESTIONTYPE_ID)
                {
                    jog = j;
                }
            }
            if (jog != null)
                QUESTIONtipusok.Remove(jog);
            throw new ArgumentException("NO DATA");
        }

        public void ClassAdd(CLASS osztaly)
        {
            osztaly.CLASS_ID = jogosultsagok.Count;
            osztalyok.Add(osztaly);
        }

        public void ClassModify(CLASS osztaly)
        {
            foreach (CLASS j in osztalyok)
            {
                if (j.CLASS_ID == osztaly.CLASS_ID)
                {
                    j.CLASS_NAME = osztaly.CLASS_NAME;
                }
            }
        }

        public void ClassDelete(CLASS osztaly)
        {
            CLASS jog = null;
            foreach (CLASS j in osztalyok)
            {
                if (j.CLASS_ID == osztaly.CLASS_ID)
                {
                    jog = j;
                }
            }
            if (jog != null)
                osztalyok.Remove(jog);
            throw new ArgumentException("NO DATA");
        }

        public IQueryable<AUTHORITY> AuthoritiesGetAll()
        {
            return jogosultsagok.AsQueryable();
        }

        public IQueryable<QUESTIONTYPE> QuestionTypesGetAll()
        {
            return QUESTIONtipusok.AsQueryable();
        }

        IQueryable<CLASS> OsztalyokListazasa()
        {
            return osztalyok.AsQueryable();
        }

        IQueryable<CLASS> ILogic.ClassesGetAll()
        {
            return osztalyok.AsQueryable();
        }

        public void QuestionAdd(QUESTION QUESTION, List<ANSWER> valaszok)
        {
            QUESTION.QUESTION_ID = QUESTIONek.Count ;
            int seged = (int)QUESTION.QUESTION_ID;
            QUESTIONek.Add(QUESTION);
            foreach (ANSWER v in valaszok)
            {
                v.ANSWER_ID = valaszok.Count;
                v.ANSWER_QUESTION_ID = seged;
                valaszok.Add(v);
            }
        }

        public void QuestionModify(QUESTION QUESTION, List<ANSWER> bejovovalaszok)
        {
            QUESTION temp = null;
            foreach (QUESTION j in QUESTIONek)
            {
                if (j.QUESTION_ID == QUESTION.QUESTION_ID)
                {
                    temp = j;
                }
            }
            temp = QUESTION;
            foreach (var item in valaszok)
            {
                if (item.ANSWER_QUESTION_ID == QUESTION.QUESTION_ID)
                    valaszok.Remove(item);
            }
            foreach (var item in bejovovalaszok)
            {
                valaszok.Add(item);
            }
        }


        public void QuestionDelete(QUESTION QUESTION, List<ANSWER> torlendovalaszok)
        {
            QUESTIONek.Remove(QUESTION);
            foreach (var item in torlendovalaszok)
            {
                valaszok.Remove(item);
            }
        }

        public IQueryable<QUESTION> QuestionsGetAll()
        {
            return QUESTIONek.AsQueryable();
        }

      

        public SUBJECT SubjectGetById(int tantargy_id)
        {
            return tantargyak[tantargy_id];
        }

        public EXERCISE ExerciseGetById(int dolgozat_id)
        {
            return dolgozatok[dolgozat_id];
        }

        public RESPONSE ResponseGetById(int RESPONSE_id)
        {
            return RESPONSEok[RESPONSE_id];
        }

        public AUTHORITY AuthorityGetById(int jogosultsag_id)
        {
            return jogosultsagok[jogosultsag_id];
        }

        public QUESTIONTYPE QuestionTypeGetById(int QUESTIONTipus_id)
        {
            return QUESTIONtipusok[QUESTIONTipus_id];
        }

        public CLASS ClassGetById(int osztaly_id)
        {
            return osztalyok[osztaly_id];
        }

        public QUESTION QuestionGetById(int QUESTION_id)
        {
            return QUESTIONek[QUESTION_id];
        }

        public ANSWER AnswerGetById(int valasz_id)
        {
            return valaszok[valasz_id];
        }

        public void ResponseDelete(RESPONSE RESPONSE)
        {
            RESPONSEok.Remove(RESPONSE);
        }

        public IQueryable<RESPONSE> ResponsesGetAll()
        {
            return RESPONSEok.AsQueryable();
        }

        public void ExerciseAdd(EXERCISE dolgozat)
        {
            dolgozat.EXERCISE_ID = dolgozatok.Count;
            dolgozatok.Add(dolgozat);
        }

        public void ExerciseModify(EXERCISE dolgozat)
        {
            EXERCISE b = null;
            foreach (var item in dolgozatok)
            {
                if (item.EXERCISE_ID == dolgozat.EXERCISE_ID)
                    b = item;
            }
            if (b != null)
            {
                dolgozatok.Remove(b);
                dolgozatok.Add(dolgozat);
            }
        }

        public IQueryable<EXERCISE> ExercisesGetAll()
        {
            return dolgozatok.AsQueryable();
        }

        public IQueryable<EXERCISE> CompletedExercisesByUser(USER USER)
        {
            List<EXERCISE> r = new List<EXERCISE>();
            foreach (var item in RESPONSEok)
            {
                if ((bool)item.RESPONSE_ISFINISHED)
                {
                    r.Add(dolgozatok[(int)item.CONNTABLE_QUESTION_EXERCISE.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID]);
                }
            }
            return r.AsQueryable();
        }

        public void ModifyAchievedScore(int RESPONSE_id, int ujPontszam)
        {
            foreach (var item in RESPONSEok)
            {
                if (item.RESPONSE_ID == RESPONSE_id)
                    item.RESPONSE_SCOREACHIEVED = ujPontszam;
            }
        }

        public IQueryable<RESPONSE> ResponseGetByExerciseAndQuestion(EXERCISE dolgozat, QUESTION QUESTION)
        {
            CONNTABLE_QUESTION_EXERCISE c = null; ;
            foreach (var item in connTable)
            {
                if (item.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID == dolgozat.EXERCISE_ID && item.CONNTABLE_QUESTION_EXERCISE_QUESTION_ID == QUESTION.QUESTION_ID)
                    c = item;
            }
            if (c == null)
                throw new ArgumentException("NINCS ILYEN RESPONSE");
            List<RESPONSE> r = new List<RESPONSE>();
            foreach (var item in RESPONSEok)
            {
                if (item.RESPONSE_CONNTABLE_ID == c.CONNTABLE_QUESTION_EXERCISE_ID)
                    r.Add(item);
            }
            return r.AsQueryable();
        }

        public EXERCISE ExerciseGetByResponse(RESPONSE RESPONSE)
        {
            CONNTABLE_QUESTION_EXERCISE c = null;
            foreach (var item in connTable)
            {
                if (item.CONNTABLE_QUESTION_EXERCISE_ID == RESPONSE.RESPONSE_CONNTABLE_ID)
                    c = item;
            }
            if (c == null)
                throw new ArgumentException("NO DATA");
            else
            {
                foreach (var item in dolgozatok)
                {
                    if (item.EXERCISE_ID == c.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID)
                        return item;
                }
            }
            throw new ArgumentException("NO DATA");
        }

        public void ExerciseAdd(EXERCISE newExercise, List<QUESTION> questions)
        {
            throw new NotImplementedException();
        }

        public void ExerciseOnlyModify(EXERCISE modifiedExercise)
        {
            throw new NotImplementedException();
        }

        public void ExerciseWithQuestionsModify(EXERCISE modifiedExercise, List<QUESTION> questions)
        {
            throw new NotImplementedException();
        }

        public IQueryable<RESPONSE> GetTextTypeResponse(EXERCISE exercise)
        {
            throw new NotImplementedException();
        }

        public void AddQuestionsToExercise(EXERCISE exercise, List<QUESTION> questions)
        {
            throw new NotImplementedException();
        }
    }
}
