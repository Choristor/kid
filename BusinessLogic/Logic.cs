﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DATA;
using System.Security.Cryptography;

namespace BusinessLogic
{
    public class Logic : ILogic
    {
        Repository repo;
        KID_DBEntities KIDDB = new KID_DBEntities();
        bool isStarted = false;

        private static Logic singleton = null;


        public static ILogic getInstance()
        {
            if (singleton == null)
                singleton = new Logic();
            return singleton;
        }

        private Logic()
        {

            ResponseRepository r1 = new ResponseRepository(KIDDB);
            ConnTableRepository r2 = new ConnTableRepository(KIDDB);
            ExerciseRepository r3 = new ExerciseRepository(KIDDB);
            UserRepository r4 = new UserRepository(KIDDB);
            QuestionRepository r5 = new QuestionRepository(KIDDB);
            SubjectRepository r6 = new SubjectRepository(KIDDB);
            AnswerRepository r7 = new AnswerRepository(KIDDB);
            AuthorityRepository r8 = new AuthorityRepository(KIDDB);
            QuestionTypeRepository r9 = new QuestionTypeRepository(KIDDB);
            ClassRepository r10 = new ClassRepository(KIDDB);
            repo = new Repository(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10);
            if(!isStarted)
                DataLoader();
            

        }

        private void DataLoader()
        {
            isStarted = true;
            ClassAdd(new CLASS() { CLASS_NAME = "1/a" });
            ClassAdd(new CLASS() { CLASS_NAME = "2/a" });

            AuthorityAdd(new AUTHORITY() { AUTHORITY_NAME = "ADMIN" });
            AuthorityAdd(new AUTHORITY() { AUTHORITY_NAME = "TANÁR" });
            AuthorityAdd(new AUTHORITY() { AUTHORITY_NAME = "DIÁK" });

            SubjectAdd(new SUBJECT() { SUBJECT_NAME = "MATEK" });
            SubjectAdd(new SUBJECT() { SUBJECT_NAME = "MAGYAR" });

            QuestionTypeAdd( new QUESTIONTYPE() { QUESTIONTYPE_NAME = "IGAZ-HAMIS" });
            QuestionTypeAdd(new QUESTIONTYPE() { QUESTIONTYPE_NAME = "FELELETVÁLASZTÓS" });
            QuestionTypeAdd(new QUESTIONTYPE() { QUESTIONTYPE_NAME = "FELELETVÁLASZTÓS-TÖBBJÓ" });
            QuestionTypeAdd(new QUESTIONTYPE() { QUESTIONTYPE_NAME = "ESSZÉ" });

            UserAdd(new USER()
            {
                USER_NAME = "TanarMatek",
                USER_PASSWORD = "bela",
                USER_AUTH = 2,
                USER_CLASS = 1,
                USER_BDATE = DateTime.Now
            });
            UserAdd(new USER()
            {
                USER_NAME = "TanarMagyar",
                USER_PASSWORD = "bela",
                USER_AUTH = 2,
                USER_CLASS = 1,
                USER_BDATE = DateTime.Now
            });
            UserAdd(new USER()
            {
                USER_NAME = "Diak1",
                USER_PASSWORD = "bela",
                USER_AUTH = 3,
                USER_CLASS = 1,
                USER_BDATE = DateTime.Now
            });
            UserAdd(new USER()
            {
                USER_NAME = "Diak2",
                USER_PASSWORD = "bela",
                USER_AUTH = 3,
                USER_CLASS = 1,
                USER_BDATE = DateTime.Now
            } );
            UserAdd(new USER()
            {
                USER_NAME = "Diak3",
                USER_PASSWORD = "bela",
                USER_AUTH = 3,
                USER_CLASS = 2,
                USER_BDATE = DateTime.Now
            } );

            QuestionAdd(new QUESTION()
            {
                QUESTION_NAME = "Teszt-igazhamis",
                QUESTION_QUESTIONTYPE_ID = 1,
                QUESTION_UPLOADERUSER_ID = 1,
                QUESTION_SCORE = 1,
                QUESTION_SUBJECT_ID = 1,
                QUESTION_CONTENT = "Igaz-e, hogy minden négyzet téglalap?"
            }, new List<ANSWER>()
            {
                new ANSWER() {ANSWER_CONTENT="IGAZ", ANSWER_ISCORRECT=true },
                new ANSWER() {ANSWER_CONTENT="HAMIS", ANSWER_ISCORRECT=false }
            });

            QuestionAdd(new QUESTION()
            {
                QUESTION_NAME = "Teszt-ABCD",
                QUESTION_QUESTIONTYPE_ID = 2,
                QUESTION_UPLOADERUSER_ID = 1,
                QUESTION_SCORE = 4,
                QUESTION_SUBJECT_ID = 1,
                QUESTION_CONTENT = "Melyik állítás igaz a négyzetre?"
            }, new List<ANSWER>()
            {
                new ANSWER() {ANSWER_CONTENT="Minden oldala egyenlő.", ANSWER_ISCORRECT=true },
                new ANSWER() {ANSWER_CONTENT="Öt szöge van.", ANSWER_ISCORRECT=false },
                new ANSWER() {ANSWER_CONTENT="Hat szöge van.", ANSWER_ISCORRECT=false },
                new ANSWER() {ANSWER_CONTENT="Nincs derékszöge.", ANSWER_ISCORRECT=false }
            });

            QuestionAdd(new QUESTION()
            {
                QUESTION_NAME = "Teszt-ABCD_több",
                QUESTION_QUESTIONTYPE_ID = 3,
                QUESTION_UPLOADERUSER_ID = 1,
                QUESTION_SCORE = 4,
                QUESTION_SUBJECT_ID = 1,
                QUESTION_CONTENT = "Melyik állítás igaz a négyzetre?"
            }, new List<ANSWER>()
            {
                new ANSWER() {ANSWER_CONTENT="Minden oldala egyenlő.", ANSWER_ISCORRECT=true },
                new ANSWER() {ANSWER_CONTENT="Négy szöge van.", ANSWER_ISCORRECT=true },
                new ANSWER() {ANSWER_CONTENT="Hat szöge van.", ANSWER_ISCORRECT=false },
                new ANSWER() {ANSWER_CONTENT="Nincs derékszöge.", ANSWER_ISCORRECT=false }
            });

            QuestionAdd(new QUESTION()
            {
                QUESTION_NAME = "Teszt-Esszé",
                QUESTION_QUESTIONTYPE_ID = 4,
                QUESTION_UPLOADERUSER_ID = 2,
                QUESTION_SCORE = 25,
                QUESTION_SUBJECT_ID = 2,
                QUESTION_CONTENT = "Jellemezd a Petőfi költészetét 5000 karakterben."
            }, null);
            
            ExerciseAdd(new EXERCISE()
            {
                EXERCISE_NAME = "Tesztdolgozat I.",
                EXERCISE_SUBJECT_ID = 1,
                EXERCISE_TIME = 5,
                EXERCISE_UPLOADERUSER_ID = 1,
            }, new List<QUESTION>()
            {
                repo.QuestionRepo.GetById(1),
                repo.QuestionRepo.GetById(2),
                repo.QuestionRepo.GetById(3)
            });

            ExerciseAdd(new EXERCISE()
            {
                EXERCISE_NAME = "Tesztdolgozat II.",
                EXERCISE_SUBJECT_ID = 2,
                EXERCISE_TIME =2,
                EXERCISE_UPLOADERUSER_ID = 2,
            }, new List<QUESTION>()
            {
                repo.QuestionRepo.GetById(4),

            });
            
        }

        #region USER-metódusok
        public USER Login(string userName, string password)
        {
            USER temp = repo.UserRepo.GetByName(userName);
            byte[] hashByte = Convert.FromBase64String(temp.USER_PASSWORD);
            byte[] salt = new byte[16];
            Array.Copy(hashByte, 0, salt, 0, 16);
            var pb = new Rfc2898DeriveBytes(password, salt, 1000);
            byte[] hash = pb.GetBytes(20);
            for (int i = 0; i < 20; i++)
            {
                if(hashByte[i+16] != hash[i])
                    throw new ArgumentException("ROSSZ JELSZÓ");
            }
            return temp;
                
        }

        public void UserAdd(USER user)
        {
            repo.UserRepo.Insert(user);
        }

        public void UserModify(USER user)
        {
            repo.UserRepo.Modify(user);
        }

        public USER UserGetById(int userId)
        {
            return repo.UserRepo.GetById(userId);
        }

        public void UserDelete(USER user)
        {
            repo.UserRepo.Delete((int)user.USER_ID);
        }

        public IQueryable<USER> GetStudentsByClass(CLASS melyikOsztaly)
        {
            return repo.UserRepo.Get(x => (x.USER_CLASS == melyikOsztaly.CLASS_ID && x.USER_AUTH == 3));
        }

        #endregion

        #region tantárgy-metódusok
        public void SubjectAdd(SUBJECT tantargy)
        {
            repo.SubjectRepo.Insert(tantargy);
        }

        public void SubjectModify(SUBJECT tantargy)
        {
            repo.SubjectRepo.Modify(tantargy);
        }

        public void SubjectDelete(SUBJECT tantargy)
        {
            repo.SubjectRepo.Delete((int)tantargy.SUBJECT_ID);
        }

        public IQueryable<SUBJECT> SubjectsGetAll()
        {
            return repo.SubjectRepo.GetAll();
        }

        public SUBJECT SubjectGetById(int tantargy_id)
        {
            return repo.SubjectRepo.GetById(tantargy_id);
        }
        #endregion

        #region jogosultság-metódusok

        public void AuthorityAdd(AUTHORITY jogosultsag)
        {
            repo.AuthorityRepo.Insert(jogosultsag);
        }

        public void AuthorityModify(AUTHORITY jogosultsag)
        {
            repo.AuthorityRepo.Modify(jogosultsag);
        }

        public void AuthorityDelete(AUTHORITY jogosultsag)
        {
            repo.AuthorityRepo.Delete((int)jogosultsag.AUTHORITY_ID);
        }

        public IQueryable<AUTHORITY> AuthoritiesGetAll()
        {
            return repo.AuthorityRepo.GetAll();
        }

        public AUTHORITY AuthorityGetById(int jogosultsag_id)
        {
            return repo.AuthorityRepo.GetById(jogosultsag_id);
        }
        #endregion

        #region osztály-metódusok

        public void ClassAdd(CLASS osztaly)
        {
            repo.ClassRepo.Insert(osztaly);
        }

        public void ClassModify(CLASS osztaly)
        {
            repo.ClassRepo.Modify(osztaly);
        }

        public void ClassDelete(CLASS osztaly)
        {
            repo.ClassRepo.Delete((int)osztaly.CLASS_ID);
        }

        public IQueryable<CLASS> ClassesGetAll()
        {
            return repo.ClassRepo.GetAll();
        }

        public CLASS ClassGetById(int osztaly_id)
        {
            return repo.ClassRepo.GetById(osztaly_id);
        }
        #endregion

        #region kérdéstípus-metódusok
        public void QuestionTypeAdd(QUESTIONTYPE QUESTIONtipus)
        {
            repo.QuestionTypeRepo.Insert(QUESTIONtipus);
        }

        public void QuestionTypeModify(QUESTIONTYPE QUESTIONtipus)
        {
            repo.QuestionTypeRepo.Modify(QUESTIONtipus);
        }

        public void QuestionTypeDelete(QUESTIONTYPE QUESTIONtipus)
        {
            repo.QuestionTypeRepo.Delete((int)QUESTIONtipus.QUESTIONTYPE_ID);
        }

        public IQueryable<QUESTIONTYPE> QuestionTypesGetAll()
        {
            return repo.QuestionTypeRepo.GetAll();
        }

        public QUESTIONTYPE QuestionTypeGetById(int QUESTIONTipus_id)
        {
            return repo.QuestionTypeRepo.GetById(QUESTIONTipus_id);
        }

        #endregion
       
        #region kérdés-metódusok

        public void QuestionAdd(QUESTION question, List<ANSWER> answers)
        {
            repo.QuestionRepo.Insert(question);
            decimal id = repo.QuestionRepo.Get(x => x.QUESTION_NAME == question.QUESTION_NAME).ToArray()[0].QUESTION_ID;
            if(answers != null)
                foreach (ANSWER v in answers)
                {
                    v.ANSWER_QUESTION_ID = id;
                    repo.AnswerRepo.Insert(v);
                }
        }

        public void QuestionModify(QUESTION QUESTION, List<ANSWER> valaszok)
        {
            // kivételt dob ha valamelyik kamu--> nincs félig módosított kérdés
            if( repo.QuestionRepo.GetById((int)QUESTION.QUESTION_ID) == null)
                throw new ArgumentException("NINCS ILYEN KÉRDÉS");
            foreach (ANSWER v in valaszok)
            {
                if (repo.AnswerRepo.GetById((int)v.ANSWER_ID) == null)
                    throw new ArgumentException("NINCS ILYEN VÁLASZ, ID: " + v.ANSWER_ID);
            }
            repo.QuestionRepo.Modify(QUESTION);
            foreach (ANSWER v in valaszok)
            {
                repo.AnswerRepo.Modify(v);
            }
        }

        public void QuestionDelete(QUESTION QUESTION, List<ANSWER> valaszok)
        {
            // kivételt dob ha valamelyik kamu--> nincs félig törölt kérdés, lehet fölösleges, de inkább legyen itt
            if (repo.QuestionRepo.GetById((int)QUESTION.QUESTION_ID) == null)
                throw new ArgumentException("NINCS ILYEN KÉRDÉS");
            foreach (ANSWER v in valaszok)
            {
                if (repo.AnswerRepo.GetById((int)v.ANSWER_ID) == null)
                    throw new ArgumentException("NINCS ILYEN VÁLASZ, ID: " + v.ANSWER_ID);
            }
            repo.QuestionRepo.Delete((int)QUESTION.QUESTION_ID);
            foreach (ANSWER v in valaszok)
            {
                repo.AnswerRepo.Delete((int)v.ANSWER_ID);
            }
        }


        public IQueryable<QUESTION> QuestionsGetAll()
        {
            return repo.QuestionRepo.GetAll();
        }

        public IQueryable<QUESTION> GetRandomQuestions(int hanyDarabot, SUBJECT melyikTargybol)
        {
            List<QUESTION> pool = repo.QuestionRepo.GetAll().ToList();
            List<QUESTION> r = new List<QUESTION>();
            int i = 0;
            int db = 0;
            while(db<hanyDarabot && i < pool.Count)
            {
                if(pool[i].QUESTION_SUBJECT_ID == melyikTargybol.SUBJECT_ID)
                {
                    r.Add(pool[i]);
                    db++;
                }
                i++;
            }
            return r.AsQueryable();
        }

        public IQueryable<QUESTION> QuestionsGetByExercise(EXERCISE dolgozat)
        {
            List<QUESTION> r = new List<QUESTION>();
            List<CONNTABLE_QUESTION_EXERCISE> pool = repo.ConntableRepo.GetAll().ToList();
            foreach (CONNTABLE_QUESTION_EXERCISE k in pool)
            {
                if (k.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID == dolgozat.EXERCISE_ID)
                    r.Add(repo.QuestionRepo.GetById((int)k.CONNTABLE_QUESTION_EXERCISE_QUESTION_ID));
            }
            return r.AsQueryable();
        }

        public QUESTION QuestionGetById(int QUESTION_id)
        {
            return repo.QuestionRepo.GetById(QUESTION_id);
        }


        #endregion

        #region Válasz-metódusok

        public IQueryable<ANSWER> AnswersGetByQuestion(QUESTION whichQuestion)
        {
            return repo.AnswerRepo.Get(x => x.ANSWER_QUESTION_ID == whichQuestion.QUESTION_ID);
        }

        public ANSWER AnswerGetById(int valasz_id)
        {
            return repo.AnswerRepo.GetById(valasz_id);
        }

        #endregion

        #region beirtVálasz-metódusok

        public void ResponseAdd(RESPONSE response, int questionId, int exerciseId)
        {                        
            response.RESPONSE_CONNTABLE_ID = repo.ConntableRepo.Get(x=>(x.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID==exerciseId && x.CONNTABLE_QUESTION_EXERCISE_QUESTION_ID==questionId)).SingleOrDefault().CONNTABLE_QUESTION_EXERCISE_ID;
            repo.ResponseRepo.Insert(response);
        }

        public void ResponseModify(RESPONSE response)
        {
            repo.ResponseRepo.Modify(response);
        }

        public void ResponseDelete(RESPONSE response)
        {
            repo.ResponseRepo.Delete((int)response.RESPONSE_ID);
        }

        public IQueryable<RESPONSE> ResponsesGetAll()
        {
            return repo.ResponseRepo.GetAll();
        }

        public IQueryable<RESPONSE> ResponsesGetByUser(USER user)
        {
            List<RESPONSE> pool = ResponsesGetAll().ToList();
            List<RESPONSE> r = new List<RESPONSE>();
            foreach (var item in pool)
            {
                if (item.RESPONSE_STUDENTUSER_ID == user.USER_ID)
                    r.Add(item);
            }
            return r.AsQueryable();
        }

        public void ModifyAchievedScore(int responseId, int ujPontszam)
        {
            RESPONSE temp = repo.ResponseRepo.GetById(responseId);
            temp.RESPONSE_SCOREACHIEVED = ujPontszam;
            repo.ResponseRepo.Modify(temp);
        }

        public int IsCorrect(RESPONSE response)
        {
            
            List <ANSWER> v  = repo.QuestionRepo.GetById((int)response.CONNTABLE_QUESTION_EXERCISE.CONNTABLE_QUESTION_EXERCISE_QUESTION_ID).ANSWER.ToList();
            if (response.CONNTABLE_QUESTION_EXERCISE.QUESTION.QUESTIONTYPE.QUESTIONTYPE_ID==4)
                throw new ArgumentException("ESSZÉ JELLEGŰ FELADATOT CSAK KÉZZEL LEHET KIJAVÍTANI");
            else if (response.CONNTABLE_QUESTION_EXERCISE.QUESTION.QUESTIONTYPE.QUESTIONTYPE_ID == 1 || response.CONNTABLE_QUESTION_EXERCISE.QUESTION.QUESTIONTYPE.QUESTIONTYPE_ID == 2)
                foreach (ANSWER item in v)
                {
                    if (item.ANSWER_CONTENT == response.RESPONSE_CONTENT)
                        return (int)item.QUESTION.QUESTION_SCORE;
                }            
            else if (response.CONNTABLE_QUESTION_EXERCISE.QUESTION.QUESTIONTYPE.QUESTIONTYPE_ID == 3)
            {
                string[] responseArray = response.RESPONSE_CONTENT.Split('\u263A');
                int resultPoints = 0;
                foreach (ANSWER item in v)
                {
                    foreach (string s in responseArray)
                    {
                        if (item.ANSWER_CONTENT == s)
                            resultPoints++;
                    }


                }
                return resultPoints;
            }
            
            throw new ArgumentException("HIBÁS BEMENET");
        }

        public RESPONSE ResponseGetById(int responseId)
        {
            return repo.ResponseRepo.GetById(responseId);
        }

        public IQueryable<RESPONSE> ResponseGetByExerciseAndQuestion(EXERCISE exercise, QUESTION question)
        {
            CONNTABLE_QUESTION_EXERCISE c = repo.ConntableRepo.Get(x => x.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID == exercise.EXERCISE_ID && x.CONNTABLE_QUESTION_EXERCISE_QUESTION_ID == question.QUESTION_ID).SingleOrDefault();
            return repo.ResponseRepo.Get(x => x.RESPONSE_CONNTABLE_ID == c.CONNTABLE_QUESTION_EXERCISE_ID);
        }

        public IQueryable<RESPONSE> GetTextTypeResponse(EXERCISE exercise)
        {
            List<RESPONSE> r = exercise.CONNTABLE_QUESTION_EXERCISE.SingleOrDefault().RESPONSE.ToList();
            List<RESPONSE> result = new List<RESPONSE>();
            foreach (var item in r)
            {
                if(item.CONNTABLE_QUESTION_EXERCISE.QUESTION.QUESTION_QUESTIONTYPE_ID == 4)
                {
                    result.Add(item);
                }
            }
            if (result == null)
                throw new ArgumentException("NEM TARTOZIK ESSZÉ KÉRDÉS A DOLGOZATHOZ");
            return result.AsQueryable();

        }

        #endregion

        #region Dolgozat-metódusok  

        public void ExerciseAdd(EXERCISE exercise, List<QUESTION> questions)
        {
            repo.ExerciseRepo.Insert(exercise);
            int exId = (int)repo.ExerciseRepo.Get(x => x.EXERCISE_NAME == exercise.EXERCISE_NAME).ToList().SingleOrDefault().EXERCISE_ID;
            if (questions == null)
                return;
            foreach (QUESTION item in questions)
            {
                if(repo.ConntableRepo.Get(x=>(x.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID== exId && x.CONNTABLE_QUESTION_EXERCISE_QUESTION_ID == item.QUESTION_ID)).Count()==0)
                    repo.ConntableRepo.Insert(new CONNTABLE_QUESTION_EXERCISE()
                    {
                        CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID=exId,
                        CONNTABLE_QUESTION_EXERCISE_QUESTION_ID=item.QUESTION_ID
                    });
            }
        }

        public void ExerciseOnlyModify(EXERCISE dolgozat)
        {
            repo.ExerciseRepo.Modify(dolgozat);
        }

        public void ExerciseWithQuestionsModify(EXERCISE modifiedExercise, List<QUESTION> questions)
        {
            repo.ExerciseRepo.Modify(modifiedExercise);
            var connList = repo.ConntableRepo.Get(x => x.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID == modifiedExercise.EXERCISE_ID);
            if (connList.Count()>0)
                foreach (var item in connList)
                {
                    repo.ConntableRepo.Delete(item);
                }
            if (questions == null)
                return;
            foreach (QUESTION q in questions)
            {
                repo.ConntableRepo.Insert(new CONNTABLE_QUESTION_EXERCISE()
                {
                    CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID = modifiedExercise.EXERCISE_ID,
                    CONNTABLE_QUESTION_EXERCISE_QUESTION_ID = q.QUESTION_ID
                });
            }
        }

        public IQueryable<EXERCISE> ExercisesGetAll()
        {
            return repo.ExerciseRepo.GetAll();
        }

        public void ExerciseDelete(EXERCISE torlendoDolgozat)
        {
            repo.ExerciseRepo.Delete(torlendoDolgozat);
            var connList = repo.ConntableRepo.Get(x => x.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID == torlendoDolgozat.EXERCISE_ID);
            if(connList.Count()>0)
                foreach (var item in connList)
                {
                    repo.ConntableRepo.Delete(item);
                }
        }

        public EXERCISE ExerciseGetByName(string azonosito)
        {
            List<EXERCISE> d =  repo.ExerciseRepo.Get(x => x.EXERCISE_NAME == azonosito).ToList();
            if (d.Count > 0)
                return d[0];
            throw new ArgumentException("NINCS ILYEN DOLGOZAT");
        } 

        public IQueryable<EXERCISE> ExercisesGetByUser(USER USER)
        {
            IQueryable<EXERCISE> d = repo.ExerciseRepo.Get(x => x.EXERCISE_UPLOADERUSER_ID == USER.USER_ID);
            if (d.Count() > 0)
                return d.AsQueryable();
            throw new ArgumentException("NINCS ILYEN DOLGOZAT");
        }

        public EXERCISE ExerciseGetById(int dolgozat_id)
        {
            List<EXERCISE> d = repo.ExerciseRepo.Get(x => x.EXERCISE_ID == dolgozat_id).ToList();
            if (d.Count() > 0)
                return d[0];
            throw new ArgumentException("NINCS ILYEN DOLGOZAT");
        }

        public IQueryable<EXERCISE> CompletedExercisesByUser(USER USER)
        {
            List<EXERCISE> d = new List<EXERCISE>();
            EXERCISE temp;
            IQueryable<RESPONSE> pool = repo.ResponseRepo.GetAll();
            foreach (var item in pool)
            {
                if((bool)item.RESPONSE_ISFINISHED && item.RESPONSE_STUDENTUSER_ID == USER.USER_ID)
                {
                    temp = repo.ExerciseRepo.GetById((int)item.CONNTABLE_QUESTION_EXERCISE.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID);
                    if (!d.Contains(temp))
                        d.Add(temp);
                }
            }
            return d.AsQueryable();            
        }

        public EXERCISE ExerciseGetByResponse(RESPONSE RESPONSE)
        {
            return repo.ExerciseRepo.Get(x => x.EXERCISE_ID == RESPONSE.CONNTABLE_QUESTION_EXERCISE.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID).SingleOrDefault();
        }

        public void AddQuestionsToExercise(EXERCISE exercise, List<QUESTION> questions)
        {
            foreach (QUESTION q in questions)
            {
                if (repo.ConntableRepo.Get(x => (x.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID == exercise.EXERCISE_ID && x.CONNTABLE_QUESTION_EXERCISE_QUESTION_ID == q.QUESTION_ID)).Count() ==0)
                    repo.ConntableRepo.Insert(new CONNTABLE_QUESTION_EXERCISE()
                        {
                            CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID=exercise.EXERCISE_ID,
                            CONNTABLE_QUESTION_EXERCISE_QUESTION_ID=q.QUESTION_ID
                        });
            }
        }



        #endregion
    }
}
