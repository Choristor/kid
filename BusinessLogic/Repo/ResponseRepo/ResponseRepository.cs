﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DATA;
namespace BusinessLogic
{
    class ResponseRepository : GenericRepository<RESPONSE>, IResponseRepository
    {
        public ResponseRepository(DbContext newctx) : base(newctx)
        {
        }

        public override RESPONSE GetById(int id)
        {
            return Get(akt => akt.RESPONSE_ID == id).SingleOrDefault();
        }


        public void Modify(RESPONSE RESPONSE)
        {
            RESPONSE akt = GetById((int)RESPONSE.RESPONSE_ID);
            if (akt == null) throw new ArgumentException("NO DATA");
            akt.RESPONSE_CONTENT = RESPONSE.RESPONSE_CONTENT;
            akt.RESPONSE_SCOREACHIEVED = RESPONSE.RESPONSE_SCOREACHIEVED;
            akt.RESPONSE_ISFINISHED = RESPONSE.RESPONSE_ISFINISHED;
            akt.RESPONSE_CONNTABLE_ID = RESPONSE.RESPONSE_CONNTABLE_ID;
            akt.RESPONSE_STUDENTUSER_ID = RESPONSE.RESPONSE_STUDENTUSER_ID;
            context.SaveChanges();
        }
    }
}
