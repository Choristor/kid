﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DATA;

namespace BusinessLogic
{
    class QuestionRepository : GenericRepository<QUESTION>, IQuestionRepository
    {
        public QuestionRepository(DbContext newctx) : base(newctx)
        {
        }

        public override QUESTION GetById(int id)
        {
            return Get(akt => akt.QUESTION_ID == id).SingleOrDefault();
        }

        public void Modify(QUESTION q)
        {
            QUESTION akt = GetById((int)q.QUESTION_ID);
            if (akt == null) throw new ArgumentException("NO DATA");
            akt.QUESTION_NAME = q.QUESTION_NAME;
            akt.QUESTION_CONTENT = q.QUESTION_CONTENT;
            akt.QUESTION_QUESTIONTYPE_ID = q.QUESTION_QUESTIONTYPE_ID;
            akt.QUESTION_SCORE = q.QUESTION_SCORE;
            akt.QUESTION_SUBJECT_ID = q.QUESTION_SUBJECT_ID;
            akt.QUESTION_UPLOADERUSER_ID = q.QUESTION_UPLOADERUSER_ID;
            context.SaveChanges();
        }
    }
}
