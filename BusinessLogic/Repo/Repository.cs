﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    class Repository
    {
        internal IResponseRepository ResponseRepo { get; set; }
        internal IConnTableRepository ConntableRepo { get; set; }
        internal IExerciseRepository ExerciseRepo { get; set; }
        internal IUserRepository UserRepo { get; set; }
        internal IQuestionRepository QuestionRepo { get; set; }
        internal ISubjectRepository SubjectRepo { get; set; }
        internal IAnswerRepository AnswerRepo { get; set; }
        internal IAuthorityRepository AuthorityRepo { get; set; }
        internal IQuestionTypeRepository QuestionTypeRepo { get; set; }
        internal IClassRepository ClassRepo { get; set; }


        public Repository(IResponseRepository responseRepo, IConnTableRepository conntableRepo,IExerciseRepository exerciseRepo, 
            IUserRepository userRepo,IQuestionRepository questionRepo,ISubjectRepository subjectRepo, IAnswerRepository answerRepo,
            IAuthorityRepository authorityRepo, IQuestionTypeRepository questionTypeRepo, IClassRepository classRepo
            )
        {
            ResponseRepo = responseRepo;
            ConntableRepo = conntableRepo;
            ExerciseRepo = exerciseRepo;
            UserRepo = userRepo;
            QuestionRepo = questionRepo;
            SubjectRepo = subjectRepo;
            AnswerRepo = answerRepo;
            AuthorityRepo = authorityRepo;
            QuestionTypeRepo = questionTypeRepo;
            ClassRepo = classRepo;
        }
    }
}
