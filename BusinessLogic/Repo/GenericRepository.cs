﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BusinessLogic
{
    abstract class GenericRepository<TEntity> : IRepository<TEntity> where TEntity:class
    {
        protected DbContext context;

        public GenericRepository(DbContext newctx)
        {
            context = newctx;
        }

        public void Delete(TEntity oldentity)
        {
            context.Set<TEntity>().Remove(oldentity);
            context.Entry<TEntity>(oldentity).State = EntityState.Deleted;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            TEntity oldentity = GetById(id);
            if (oldentity == null) throw new ArgumentException("NO DATA");
            Delete(oldentity);
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<TEntity> GetAll()
        {
            return context.Set<TEntity>();
        }

        public abstract TEntity GetById(int id);

        public virtual void Insert(TEntity newentity)
        {
            if (newentity == null)
                throw new ArgumentException("NULL PARAMÉTER ERROR");
            context.Set<TEntity>().Add(newentity);
            try
            {
                context.SaveChanges();
            }
            catch (Exception)
            {
                context.Set<TEntity>().Remove(newentity);
            }
        }

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition)
        {
            return GetAll().Where(condition);
        }
    }
}
