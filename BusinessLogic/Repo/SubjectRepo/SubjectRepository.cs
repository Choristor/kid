﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DATA;

namespace BusinessLogic
{
    class SubjectRepository : GenericRepository<SUBJECT>, ISubjectRepository
    {
        public SubjectRepository(DbContext newctx) : base(newctx)
        {
        }

        public override SUBJECT GetById(int id)
        {
            return Get(akt => akt.SUBJECT_ID == id).SingleOrDefault();
        }

        public void Modify(SUBJECT tantargy)
        {
            SUBJECT akt = GetById((int)tantargy.SUBJECT_ID);
            if (akt == null) throw new ArgumentException("NINCS ILYEN TANTÁRGY");
            akt.SUBJECT_NAME = tantargy.SUBJECT_NAME;
            context.SaveChanges();
        }
    }
}
