﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DATA;

namespace BusinessLogic
{
    interface IConnTableRepository : IRepository<CONNTABLE_QUESTION_EXERCISE>
    {
        void Modify(CONNTABLE_QUESTION_EXERCISE connTable);
    }
}
