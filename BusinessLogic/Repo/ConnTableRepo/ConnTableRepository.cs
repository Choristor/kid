﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DATA;

namespace BusinessLogic
{
    class ConnTableRepository : GenericRepository<CONNTABLE_QUESTION_EXERCISE>, IConnTableRepository
    {
        public ConnTableRepository(DbContext newctx) : base(newctx)
        {
        }

        public override CONNTABLE_QUESTION_EXERCISE GetById(int id)
        {
            return Get(akt => akt.CONNTABLE_QUESTION_EXERCISE_ID == id).SingleOrDefault();
        }

        public void Modify(CONNTABLE_QUESTION_EXERCISE connTable)
        {
            CONNTABLE_QUESTION_EXERCISE akt = GetById((int)connTable.CONNTABLE_QUESTION_EXERCISE_ID);
            if (akt == null) throw new ArgumentException("NO DATA");
            akt.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID = connTable.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID;
            akt.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID = connTable.CONNTABLE_QUESTION_EXERCISE_EXERCISE_ID;
            context.SaveChanges();

        }

        public override void Insert(CONNTABLE_QUESTION_EXERCISE newentity)
        {
            base.Insert(newentity);
        }
    }
}
