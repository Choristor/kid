﻿using DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BusinessLogic
{
    class QuestionTypeRepository : GenericRepository<QUESTIONTYPE>, IQuestionTypeRepository
    {
        public QuestionTypeRepository(DbContext newctx) : base(newctx)
        {
        }

        public override QUESTIONTYPE GetById(int id)
        {
            return Get(akt => akt.QUESTIONTYPE_ID == id).SingleOrDefault();
        }

        public void Modify(QUESTIONTYPE QUESTIONtipus)
        {
            QUESTIONTYPE akt = GetById((int)QUESTIONtipus.QUESTIONTYPE_ID);
            if (akt == null) throw new ArgumentException("NO DATA");
            akt.QUESTIONTYPE_NAME = QUESTIONtipus.QUESTIONTYPE_NAME;
            context.SaveChanges();
        }
    }
}
