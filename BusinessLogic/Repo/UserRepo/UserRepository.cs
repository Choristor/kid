﻿using DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Security.Cryptography;

namespace BusinessLogic
{
    class UserRepository : GenericRepository<USER>, IUserRepository
    {
        public UserRepository(DbContext newctx) : base(newctx)
        {
        }

        public override USER GetById(int id)
        {
            return Get(akt => akt.USER_ID == id).SingleOrDefault();
        }

        public USER GetByName(string name)
        {
            return Get(akt => akt.USER_NAME == name).SingleOrDefault();
        }

        public void Modify(USER f)
        {
            USER akt = GetById((int)f.USER_ID);
            if (akt == null) throw new ArgumentException("NO DATA");
            akt.USER_NAME = f.USER_NAME;
            akt.USER_PASSWORD = Hash(f.USER_PASSWORD);
            akt.USER_CLASS = f.USER_CLASS;
            akt.USER_BDATE = f.USER_BDATE;
            akt.USER_AUTH = f.USER_AUTH;
            context.SaveChanges();
        }

        public override void Insert(USER newentity)
        {
            newentity.USER_PASSWORD = Hash(newentity.USER_PASSWORD);
            base.Insert(newentity);
        }

        private string Hash(string oldpassword)
        {
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            var pb = new Rfc2898DeriveBytes(oldpassword, salt, 1000);
            byte[] hash = pb.GetBytes(20);
            byte[] hashBytesArray = new byte[36];
            Array.Copy(salt, 0, hashBytesArray, 0, 16);
            Array.Copy(hash, 0, hashBytesArray, 16, 20);
           return Convert.ToBase64String(hashBytesArray);
        }
    }
}
