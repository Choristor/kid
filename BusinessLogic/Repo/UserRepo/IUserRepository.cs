﻿using DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    interface IUserRepository : IRepository<USER>
    {
        void Modify(USER USER);

        USER GetByName(string name);

        //IQueryable<USER> GetByOsztal(string osztaly);
    }
}
