﻿using DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BusinessLogic
{
    class AuthorityRepository : GenericRepository<AUTHORITY>, IAuthorityRepository
    {
        public AuthorityRepository(DbContext newctx) : base(newctx)
        {
        }

        public override AUTHORITY GetById(int id)
        {
            return Get(akt => akt.AUTHORITY_ID == id).SingleOrDefault();
        }

        public void Modify(AUTHORITY jogosultsag)
        {
            AUTHORITY akt = GetById((int)jogosultsag.AUTHORITY_ID);
            if (akt == null) throw new ArgumentException("NO DATA");
            akt.AUTHORITY_NAME = jogosultsag.AUTHORITY_NAME;
            context.SaveChanges();
        }
    }
}
