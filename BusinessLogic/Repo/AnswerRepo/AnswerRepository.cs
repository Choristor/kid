﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DATA;


namespace BusinessLogic
{
    class AnswerRepository : GenericRepository<ANSWER>, IAnswerRepository
    {
        public AnswerRepository(DbContext newctx) : base(newctx)
        {
        }

        public void Modify(ANSWER valasz)
        {
            ANSWER akt = GetById((int)valasz.ANSWER_ID);
            if (akt == null) throw new ArgumentException("NO DATA");
            akt.ANSWER_CONTENT = valasz.ANSWER_CONTENT;
            akt.ANSWER_QUESTION_ID = valasz.ANSWER_QUESTION_ID;
            akt.ANSWER_ISCORRECT = valasz.ANSWER_ISCORRECT;
            context.SaveChanges();
        }

        public override ANSWER GetById(int id)
        {
            return Get(akt => akt.ANSWER_ID == id).SingleOrDefault();
        }
    }
}
