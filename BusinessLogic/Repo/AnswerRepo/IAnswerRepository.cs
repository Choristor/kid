﻿using DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    interface IAnswerRepository :IRepository<ANSWER>
    {
        void Modify(ANSWER valasz);
    }
}
