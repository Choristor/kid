﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DATA;

namespace BusinessLogic
{
    class ClassRepository : GenericRepository<CLASS>, IClassRepository
    {
        public ClassRepository(DbContext newctx) : base(newctx)
        {
        }

        public override CLASS GetById(int id)
        {
            return Get(akt => akt.CLASS_ID == id).SingleOrDefault();
        }

        public void Modify(CLASS osztaly)
        {
            CLASS akt = GetById((int)osztaly.CLASS_ID);
            if (akt == null) throw new ArgumentException("NO DATA");
            akt.CLASS_NAME = osztaly.CLASS_NAME;
            context.SaveChanges();
        }
    }
}
