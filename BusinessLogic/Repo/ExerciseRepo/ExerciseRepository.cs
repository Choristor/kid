﻿using DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BusinessLogic
{
    class ExerciseRepository : GenericRepository<EXERCISE>, IExerciseRepository
    {
        public ExerciseRepository(DbContext newctx) : base(newctx)
        {
        }

        public override EXERCISE GetById(int id)
        {
            return Get(akt => akt.EXERCISE_ID == id).SingleOrDefault();
        }

        public void Modify(EXERCISE dolgozat)
        {
            EXERCISE akt = GetById((int)dolgozat.EXERCISE_ID);
            if (akt == null) throw new ArgumentException("NO DATA");
            akt.EXERCISE_NAME = dolgozat.EXERCISE_NAME;
            akt.EXERCISE_TIME = dolgozat.EXERCISE_TIME;
            akt.EXERCISE_SUBJECT_ID = dolgozat.EXERCISE_SUBJECT_ID;
            akt.EXERCISE_UPLOADERUSER_ID = dolgozat.EXERCISE_UPLOADERUSER_ID;
            context.SaveChanges();
        }
    }
}
