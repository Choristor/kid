﻿using DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    interface IExerciseRepository : IRepository<EXERCISE>
    {
        void Modify(EXERCISE dolgozat);
    }
}
