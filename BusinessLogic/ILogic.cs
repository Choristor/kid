﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DATA;


namespace BusinessLogic
{

    public interface ILogic
    {
        USER Login(string userName, string password);
        void UserAdd(USER newUser);
        void UserModify(USER modifiedUser);
        void UserDelete(USER deletedUser);
        USER UserGetById(int userId);
        IQueryable<USER> GetStudentsByClass(CLASS whichClass);


        void SubjectAdd(SUBJECT newSubject);
        void SubjectModify(SUBJECT modifiedSubject);
        void SubjectDelete(SUBJECT deletedSubject);
        IQueryable<SUBJECT> SubjectsGetAll();
        SUBJECT SubjectGetById(int subjectId);



        void AuthorityAdd(AUTHORITY newAuthority);
        void AuthorityModify(AUTHORITY modifiedAuthority);
        void AuthorityDelete(AUTHORITY deleteAuthority);
        IQueryable<AUTHORITY> AuthoritiesGetAll();
        AUTHORITY AuthorityGetById(int authorityId);

        void QuestionTypeAdd(QUESTIONTYPE newQuestionType);
        void QuestionTypeModify(QUESTIONTYPE modifiedQuestionType);
        void QuestionTypeDelete(QUESTIONTYPE deletedQuestionType);
        IQueryable<QUESTIONTYPE> QuestionTypesGetAll();
        QUESTIONTYPE QuestionTypeGetById(int questionTypeId);

        void ClassAdd(CLASS newClass);
        void ClassModify(CLASS modifiedClass);
        void ClassDelete(CLASS deletedClass);
        IQueryable<CLASS> ClassesGetAll();
        CLASS ClassGetById(int classId);

        void QuestionAdd(QUESTION newQuestion, List<ANSWER> newAnswers);
        void QuestionModify(QUESTION modifiedQuestion, List<ANSWER> modifiedAnswers);
        void QuestionDelete(QUESTION deletedQuestion, List<ANSWER> deletedAnswers);
        IQueryable<QUESTION> QuestionsGetAll();
        QUESTION QuestionGetById(int questionId);
        IQueryable<QUESTION> GetRandomQuestions(int howMuch, SUBJECT whichSubject);
        IQueryable<QUESTION> QuestionsGetByExercise(EXERCISE exercise);


        IQueryable<ANSWER> AnswersGetByQuestion(QUESTION whichQuestion);
        ANSWER AnswerGetById(int answerId);
       
        void ResponseAdd(RESPONSE newResponse, int questionId, int exerciseId);
        void ResponseModify(RESPONSE modifiedResponse);
        void ResponseDelete(RESPONSE deletedResponse);
        IQueryable<RESPONSE> ResponsesGetAll();
        RESPONSE ResponseGetById(int responseId);
        IQueryable<RESPONSE> ResponsesGetByUser(USER user);
        void ModifyAchievedScore(int responseId, int ujPontszam);
        int IsCorrect(RESPONSE response);
        IQueryable<RESPONSE> ResponseGetByExerciseAndQuestion(EXERCISE exercise, QUESTION quesiton);
        IQueryable<RESPONSE> GetTextTypeResponse(EXERCISE exercise);


        void ExerciseAdd(EXERCISE newExercise, List<QUESTION> newquestions);
        void ExerciseOnlyModify(EXERCISE modifiedExercise);
        void ExerciseWithQuestionsModify(EXERCISE modifiedExercise, List<QUESTION> questions);
        void ExerciseDelete(EXERCISE deletedExercise);
        IQueryable<EXERCISE> ExercisesGetAll();
        EXERCISE ExerciseGetById(int exerciseId);
        EXERCISE ExerciseGetByName(string exerciseName);
        EXERCISE ExerciseGetByResponse(RESPONSE whichResponse);
        IQueryable<EXERCISE> ExercisesGetByUser(USER whichUser);
        IQueryable<EXERCISE> CompletedExercisesByUser(USER whichUser);

        void AddQuestionsToExercise(EXERCISE exercise, List<QUESTION> questions);
    }
}
